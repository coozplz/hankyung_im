package uclick.messenger.client.trayicon;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;

import uclick.messenger.client.MyMessengerInfo;
import uclick.messenger.client.ProtocolRequestEventHandler;
import uclick.messenger.client.dto.MemberDto;
import uclick.messenger.client.gui.MainFrame;
import uclick.messenger.client.protocol.MessageProtocol;
import uclick.messenger.client.protocol.Protocol;


public class MessengerTrayIcon implements ActionListener,Observer{

	private MainFrame mainFrame;
	
	private PopupMenu mPopup;
	private TrayIcon trayIcon;
	private SystemTray systemTray;
	private MenuItem mItemLogin, mItemLogout, mItemOpen, mItemExit;
	private MyMessengerInfo myInfo = MyMessengerInfo.getInstance();
	private ProtocolRequestEventHandler eventHandler = ProtocolRequestEventHandler.getEventHandler();
	Image offLineImage = Toolkit.getDefaultToolkit().getImage("image/offline_tray.gif");
	Image onLineImage = Toolkit.getDefaultToolkit().getImage("image/online_tray.gif");
	public MessengerTrayIcon(MainFrame mFrame){
		this.mainFrame = mFrame;
		try {
			initSystemTrayIcon();
		} catch (AWTException e) {
			
		}
	}
	public void initSystemTrayIcon() throws AWTException {         
		if (SystemTray.isSupported()) {             
			mPopup = new PopupMenu();           
			mItemLogin= new MenuItem("로그인");          
			mItemLogout = new MenuItem("로그아웃");           
			mItemOpen = new MenuItem("열기");            
			mItemExit = new MenuItem("끝내기"); 
			           
			mItemLogin.addActionListener(this);             
			mItemLogout.addActionListener(this);           
			mItemOpen.addActionListener(this);           
			mItemExit.addActionListener(this);           
			mPopup.add(mItemLogin);        
			mPopup.add(mItemLogout);
			mPopup.addSeparator();  
			mPopup.add(mItemOpen); 
			mPopup.add(mItemExit);
			mPopup.addSeparator();          
			    
			Image image = Toolkit.getDefaultToolkit().getImage("image/offline_tray.gif");          
			trayIcon = new TrayIcon(image, "UCLICK MESSENGER - 오프라인", mPopup);   
			trayIcon.setImageAutoSize(true);          
			systemTray = SystemTray.getSystemTray();          
			systemTray.add(trayIcon); 
			
			logmenuStatus(true);
		} 
	} 
	public void actionPerformed(ActionEvent ae) {    
		if (ae.getSource() == mItemLogin) {           
			loginEvent();
		} else if (ae.getSource() == mItemLogout) { 
			logoutEvent();
		} else if (ae.getSource() == mItemOpen) {  
			loginEvent();
		} else if (ae.getSource() == mItemExit) {
			logoutEvent();
			mainFrame.dispose();
			System.exit(0);   
		} 
	}  
	private void loginEvent(){
		mainFrame.setVisible(true);
		//mainFrame.setFocusable(true);
	}
	private void logoutEvent(){
		MessageProtocol protocol = new MessageProtocol();
		protocol.setProtocolData("protocol", Protocol.REQ_LOG_OUT);
		protocol.setProtocolData("socket", MainFrame.socket);
		protocol.setProtocolData("id",myInfo.getUser().getMemId());
		eventHandler.setProtocol(protocol);
	}
	public void logmenuStatus(boolean status){
		mItemLogout.setEnabled(status);
		mItemLogin.setEnabled(!status);
		//loginEvent();
	}
	public void update(Observable o, Object obj) {
		if(obj instanceof MemberDto){
			MemberDto user = (MemberDto)obj;
			if(user.getNickName().equals(myInfo.getUser().getNickName())){
				setTrayIcon(user);
			}else {
				if(user.getStatus().equals("1")){
					trayIcon.displayMessage(JOptionPane.MESSAGE_PROPERTY, 
							user.getName()+"("+user.getNickName()+")"+"님 께서 로그인 하셨습니다.", 
							MessageType.INFO);
				}
			}
		}
	}
	private void setTrayIcon(MemberDto user){
		int status = Integer.parseInt(user.getStatus());
		switch (status) {
		case 0:
			this.trayIcon.setImage(offLineImage);
			this.trayIcon.setToolTip("UCLICK MESSENGER - 오프라인");
			logmenuStatus(false);
			break;
		case 1:
			this.trayIcon.setImage(onLineImage);
			this.trayIcon.setToolTip("UCLICK MESSENGER - 온라인");
			break;
		default:
			break;
		}
	}
}
