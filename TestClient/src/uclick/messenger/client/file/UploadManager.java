package uclick.messenger.client.file;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;



public class UploadManager extends JFrame implements Observer, ActionListener {
	/**
	 * Logger for this class
	 */
	/**SSSW
	 * 
	 */
	private static final long serialVersionUID = -2956956175952852343L;
	protected UploadsTableModel tableModel;

	protected JTable table = new JTable();
	private JButton pauseButton;
	private JButton resumeButton;
	private JButton cancelButton;
	private JButton clearButton;

	private Upload selectedUpload;
	private boolean clearing;

	public static UploadManager manager;

	private UploadManager() {
		init();
	}

	public static UploadManager getInstance(){
		if(manager ==null){
			manager = new UploadManager();
		}
		return manager;
	}

	public void setlocation(){
		Dimension monitor = Toolkit.getDefaultToolkit().getScreenSize();
		int locationX = (monitor.width-640)/2;
		int locationY = (monitor.height-300)/2;
		this.setBounds(locationX, locationY, 640, 300);
	}
	public void init() {
		this.setTitle("업로드  관리자");
		this.setlocation();
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				actionExit();
			}
		});
		/* 다운로드 테이블 모델을 생성 */

		tableModel = new UploadsTableModel();
		table = new JTable(tableModel);
		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						tableSelectionChanged();
					}
				});

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		ProgressRenderer renderer = new ProgressRenderer(0, 100);
		renderer.setStringPainted(true);
		table.setDefaultRenderer(JProgressBar.class, renderer);
		table.setRowHeight((int) (renderer.getPreferredSize().getHeight()));

		JPanel downloadPanel = new JPanel();
		downloadPanel.setBorder(BorderFactory.createTitledBorder("Downloads"));
		downloadPanel.setLayout(new BorderLayout());
		downloadPanel.add(new JScrollPane(table), BorderLayout.CENTER);

		// Buttons 설정
		JPanel buttonsPanel = new JPanel();
		pauseButton = new JButton("PAUSE");
		pauseButton.setEnabled(false);
		pauseButton.addActionListener(this);
		buttonsPanel.add(pauseButton);
		

		resumeButton = new JButton("RESUME");
		resumeButton.setEnabled(false);
		resumeButton.addActionListener(this);
		buttonsPanel.add(resumeButton);

		cancelButton = new JButton("CANCEL");
		cancelButton.setEnabled(false);
		cancelButton.addActionListener(this);
		buttonsPanel.add(cancelButton);

		clearButton = new JButton("clear");
		clearButton.setEnabled(false);
		clearButton.addActionListener(this);
		buttonsPanel.add(clearButton);
		
	

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(downloadPanel, BorderLayout.CENTER);
		getContentPane().add(buttonsPanel, BorderLayout.SOUTH);
		updateButtons();
	}
	protected void tableSelectionChanged() {
		if (selectedUpload != null) {
			selectedUpload.deleteObserver(UploadManager.this);
		}
		
		if (!clearing) {
			selectedUpload = tableModel.getUpload(table.getSelectedRow());
			selectedUpload.addObserver(UploadManager.this);
			updateButtons();
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		if (selectedUpload != null && selectedUpload.equals(arg)) {
			updateButtons();

		}
	}
	protected void updateButtons() {
		if (selectedUpload != null) {
			int status = selectedUpload.getStatus();
			switch (status) {
			case Upload.UPLOADING:
				pauseButton.setEnabled(true);
				resumeButton.setEnabled(false);
				cancelButton.setEnabled(true);
				clearButton.setEnabled(false);
				break;
			case Upload.PAUSED:
				pauseButton.setEnabled(false);
				resumeButton.setEnabled(true);
				cancelButton.setEnabled(true);
				clearButton.setEnabled(false);
				break;
			case Upload.ERROR:
				pauseButton.setEnabled(false);
				resumeButton.setEnabled(true);
				cancelButton.setEnabled(false);
				clearButton.setEnabled(true);
				break;
			default: //
				pauseButton.setEnabled(false);
				resumeButton.setEnabled(false);
				cancelButton.setEnabled(false);
				clearButton.setEnabled(true);
			}
		} else {
			pauseButton.setEnabled(false);
			resumeButton.setEnabled(false);
			cancelButton.setEnabled(false);
			clearButton.setEnabled(false);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == pauseButton ) {
			actionPause();
		} else if (e.getSource()==resumeButton) {
			actionResume();
		} else if (e.getSource()==cancelButton) {
			actionCancel();
		} else if (e.getSource()==clearButton) {
			actionClear();
		}
	}
	protected void actionCancel() {
		selectedUpload.cancel();
		updateButtons();
	}

	protected void actionResume() {
		selectedUpload.resume();
		updateButtons();
	}

	protected void actionPause() {
		selectedUpload.pause();
		updateButtons();
	}
	public void actionAdd(Upload upload) {
		tableModel.addUpload(upload);
		updateButtons();
	}
	protected void actionClear() {
		clearing = true;
		tableModel.clearUpload(table.getSelectedRow());
		clearing = false;
		selectedUpload = null;
	}
	private void actionExit() {
		dispose();
	}

}
