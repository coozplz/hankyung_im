package uclick.messenger.client.file;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JProgressBar;
import javax.swing.table.AbstractTableModel;

class DownloadsTableModel extends AbstractTableModel implements Observer {
	private static final String[] columnNames = {"보내는사람", "파일명", "크기(KB)", "진행",
			"상태" };

	private static final Class[] columnClasses = { String.class ,String.class, String.class,
			JProgressBar.class, String.class };

	private ArrayList<Download> downloadList = new ArrayList<Download>();

	public void addDownload(Download download) {
		download.addObserver(this);
		downloadList.add(download);
		fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
	}

	public Download getDownload(int row) {
		return (Download) downloadList.get(row);
	}

	public void clearDownload(int row) {
		downloadList.remove(row);
		fireTableRowsDeleted(row, row);
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Class getColumnClass(int col) {
		return columnClasses[col];
	}

	public int getRowCount() {
		return downloadList.size();
	}

	public Object getValueAt(int row, int col) {
		Download download = downloadList.get(row);
		switch (col) {
		case 0:
			return download.getSender();
		case 1: // URL
			return download.getFileName();
		case 2: // Size
			int size = (int)(download.getSize()/1024);
			int downloaded = (int)(download.getDownloaded()/1024);
			return (size == -1) ? "" : "["+downloaded+" / "+size+"KB ]";
		case 3: // Progress
			return new Float(download.getProgress());
		case 4: // Status
			return Download.DWSTATUSES[download.getStatus()];
		}
		return "";
	}

	public void update(Observable o, Object arg) {
		int index = downloadList.indexOf(o);
		fireTableRowsUpdated(index, index);
	}
}
