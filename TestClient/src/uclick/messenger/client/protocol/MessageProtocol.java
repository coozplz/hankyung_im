package uclick.messenger.client.protocol;

import java.util.HashMap;


public class MessageProtocol implements Protocol{

	private HashMap<String, Object> map;
	
	public MessageProtocol() {
		
		map = new HashMap<String, Object>();
	}
	public void setProtocolData(String entity,Object message){
		
		map.put(entity, message);
	}
	public Object getContent(String key) {
		
		return map.get(key);
	}
}