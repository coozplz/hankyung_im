package uclick.messenger.client.gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import uclick.messenger.client.MyMessengerInfo;


public class MyInfoPanel extends JPanel implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3012885231589847246L;
	private JPanel imageLabel = new JPanel();
	private JPanel userInfoPanel = new JPanel();
	private ImageIcon myimage;
	private JLabel leftLabel,userName;
	private MyMessengerInfo myInfo = MyMessengerInfo.getInstance();
	JPopupMenu status;
	private String[] statusImage = {"image/online.gif","image/offline.gif"};
	private String[] statusText = {"온라인","오프라인"};
	public MyInfoPanel(){
		initAWTContainer();
	}

	private void initAWTContainer() {
		this.setLayout(new BorderLayout());
		myimage = new ImageIcon("image/myinfo.gif");
		leftLabel = new JLabel(myimage);
		leftLabel.setBorder(BorderFactory.createLoweredBevelBorder());
	
		userName = new JLabel(myInfo.getName()+"님");
		userName.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		userInfoPanel.setLayout(new BorderLayout());
		userInfoPanel.add(userName,BorderLayout.WEST);
		userInfoPanel.setBackground(ChatConstants.userColor);
		imageLabel.add(leftLabel);
		imageLabel.setBackground(ChatConstants.userColor);
		this.add(imageLabel ,BorderLayout.WEST);
		this.add(userInfoPanel,BorderLayout.CENTER);
		this.setBackground(SystemColor.controlDkShadow);
		this.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {
				
				panelEvent(false);
				if(!e.isPopupTrigger()){
					createStatusMenu(e,statusImage,statusText);
				}
				panelEvent(true);
			}

			public void mousePressed(MouseEvent e) {
				panelEvent(false);
			}

			public void mouseExited(MouseEvent e) {
				panelEvent(true);
			}

			public void mouseReleased(MouseEvent e) {
				panelEvent(true);
			}
			
		});
	}
	private void panelEvent(boolean flag){
		if(flag){
			this.setBorder(BorderFactory.createLineBorder(ChatConstants.userColor));
		}else{
			this.setBorder(BorderFactory.createLoweredBevelBorder());
		}
	}
	private void createStatusMenu(MouseEvent e,String[] data,String [] text){
		status = new JPopupMenu();
		status.setForeground(SystemColor.white);
		JMenuItem item = null;
		for (int i = 0; i < data.length; i++) {
			ImageIcon icon = new ImageIcon(data[i]);
			item = new JMenuItem(text[i],icon);
			item.setBorder(BorderFactory.createLoweredBevelBorder());
			item.addActionListener(this);
			status.add(item);
		}
		status.show(e.getComponent(), leftLabel.getWidth(),leftLabel.getHeight());
	}
	public void actionPerformed(ActionEvent e) {
		
	}
}
