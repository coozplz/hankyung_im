package uclick.messenger.client.gui;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

public class UtilPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3012885231589847246L;
	private JPanel utilUp = new JPanel();
	private JButton btn1 = new JButton("a");
	public UtilPanel(){
		initAWTContainer();
	}

	private void initAWTContainer() {
		this.setLayout(new BorderLayout());
		this.setBackground(Color.orange);
		this.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
		utilUp.add(btn1);
		this.add(utilUp);
	}
}
