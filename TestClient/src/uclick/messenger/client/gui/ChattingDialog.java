package uclick.messenger.client.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import uclick.messenger.client.MyMessengerInfo;
import uclick.messenger.client.ProtocolRequestEventHandler;
import uclick.messenger.client.protocol.MessageProtocol;
import uclick.messenger.client.protocol.Protocol;


public class ChattingDialog extends JFrame implements ActionListener,Observer{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2274584256141937695L;
	MainFrame frame;
	private JPanel textAreaPanel ;
	private JPanel textfiledPanel;
	private JPanel centerPanel;
	/////////////////////////
	
	/////////////////////////////
	private JPanel southPanel;
	private ChattingDialog thisframe;
	private Container container;
	public  JTextArea userChatt;
	private JTextArea text1 = new JTextArea(3,5);
	private JButton sendBtn = new JButton("보내기");
	boolean isFocused = false;
	private JToolBar toolBar;
	public JButton addUserBtn;
	private JButton fileBtn ;
	private MyMessengerInfo myInfo;
	String roomNum;
	private ProtocolRequestEventHandler eventHandler = ProtocolRequestEventHandler.getEventHandler();
	
	public ChattingDialog(){
		initAwtContainer();
	}
	public ChattingDialog(MainFrame frame , MyMessengerInfo myInfo , String roomNum)
	{
		this.frame = frame;
		this.myInfo = myInfo;
		this.roomNum = roomNum;
		initAwtContainer();
	}

	public void initAwtContainer()
	{
		thisframe = this;
		toolBar = new JToolBar();
		toolBar.setFocusable(false);
		toolBar.setEnabled(false);
		thisframe.setJMenuBar(new ChattMenuBar());
		container= thisframe.getContentPane();
		container.setLayout(new BorderLayout());
		/**
		 * image
		 */
		ImageIcon m1 = new ImageIcon("image/file.png");
		ImageIcon userAdd = new ImageIcon("image/user_add.png");

		textAreaPanel = new JPanel();
		textAreaPanel.setLayout(new BorderLayout());
		userChatt = new JTextArea(12,42);
		userChatt.setLineWrap(true);
		userChatt.setEditable(false);
		JScrollPane pane
		= new JScrollPane(userChatt,
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pane.setForeground(Color.white);
		pane.setBorder(BorderFactory.createEtchedBorder());
		textAreaPanel.add(pane,BorderLayout.CENTER);
		//////////////////////////////////////////////////////////////////
		centerPanel=  new JPanel();
		fileBtn = new JButton(m1);
		fileBtn.setFocusable(false);
		fileBtn.setToolTipText("파일 보내기");
		fileBtn.setBackground(SystemColor.control);
		//
		addUserBtn = new JButton(userAdd);
		addUserBtn.setToolTipText("친구 초대");
		addUserBtn.setFocusable(false);
		addUserBtn.setBackground(SystemColor.control);
		toolBar.add(addUserBtn);
		toolBar.add(fileBtn);
		toolBar.setBackground(SystemColor.control);
		textAreaPanel.add(centerPanel, BorderLayout.NORTH);
		textAreaPanel.add(new JPanel(),BorderLayout.SOUTH);
		///////////////////////////////////////////////////////////////
		southPanel = new JPanel();
		southPanel.setBackground(SystemColor.control);
		southPanel.setOpaque(false);
		//////////////////////////////////////////////////////////////////
		textfiledPanel = new JPanel();
		textfiledPanel.setLayout(new BorderLayout());
		textfiledPanel.setBorder(BorderFactory.createEtchedBorder());
		
		text1.setLineWrap(true);
		text1.setFocusable(true);
		
		JScrollPane typepane
					= new JScrollPane(text1,
							JScrollPane.VERTICAL_SCROLLBAR_NEVER,
							JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		typepane.setBorder(BorderFactory.createCompoundBorder());
		textfiledPanel.add(typepane,BorderLayout.CENTER);
		
		sendBtn.setBackground(SystemColor.control);
		sendBtn.setEnabled(false);
		sendBtn.addActionListener(this);
		textfiledPanel.add(sendBtn,BorderLayout.EAST);
		textfiledPanel.add(southPanel,BorderLayout.SOUTH);
		///////////////////////////////////////////////////////////////////
		container.add(toolBar,BorderLayout.NORTH);
		container.add(textAreaPanel , BorderLayout.CENTER);
		container.add(textfiledPanel , BorderLayout.SOUTH);
		
		thisframe.setBounds(300, 50,500, 450);
		
		text1.addKeyListener(new KeyAdapter(){

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode()==KeyEvent.VK_ENTER){
					
					String str = text1.getText();
					MessageProtocol protocol = new MessageProtocol();
					protocol.setProtocolData("socket", frame.getSocket());
					protocol.setProtocolData("protocol", Protocol.GENERAL_MSG);
					protocol.setProtocolData("roomnum", roomNum);
					protocol.setProtocolData("id",myInfo.getUser().getMemId() );
					protocol.setProtocolData("nick", myInfo.getUser().getNickName());
					protocol.setProtocolData("message", str.trim());
					eventHandler.setProtocol(protocol);
					text1.setText("");
					text1.setFocusable(true);
				}else {
					
				}
			}
		});
		text1.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				System.out.println("remove");
				sendBtn.setEnabled(false);
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				sendBtn.setEnabled(true);
				System.out.println("insert");
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		thisframe.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e)
			{
				ChattingDialog.this.setVisible(false);
			}

			public void windowActivated(WindowEvent ae) {
				isFocused = true;
				//if(timer != null) timer.stop();
			}

			public void windowDeactivated(WindowEvent ae) {
				isFocused = false;
			}
    		public void windowOpened( WindowEvent e ){
    			text1.requestFocus();
    	    }
		});
		this.setVisible(true);
		isFocused = false;
		addListener();
	}
	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if(obj==sendBtn){
			String str = text1.getText();
			MessageProtocol protocol = new MessageProtocol();
			protocol.setProtocolData("socket", frame.getSocket());
			protocol.setProtocolData("protocol", Protocol.GENERAL_MSG);
			protocol.setProtocolData("roomnum", roomNum);
			protocol.setProtocolData("id",myInfo.getUser().getMemId() );
			protocol.setProtocolData("nick", myInfo.getUser().getNickName());
			protocol.setProtocolData("message", str);
			eventHandler.setProtocol(protocol);
			text1.setText("");
			text1.setFocusable(true);
		}else if(obj==fileBtn){
			System.out.println("파일 버튼");
			
		}else if(obj==addUserBtn){
			System.out.println("친구 초대 버튼");
			MessageProtocol protocol = new MessageProtocol();
			protocol.setProtocolData("protocol", Protocol.REQ_FRIEND_LIST);
			protocol.setProtocolData("id", myInfo.getUser().getMemId());
			protocol.setProtocolData("roomnum", roomNum);
			protocol.setProtocolData("socket", frame.getSocket());
			eventHandler.setProtocol(protocol);
			//new FriendIvitedDialog(this,"친구초대");
			addUserBtn.setEnabled(false);
		}
	}
	private void addListener(){
		sendBtn.addActionListener(this);
		fileBtn.addActionListener(this);
		addUserBtn.addActionListener(this);
	}

	public void update(Observable arg0, Object arg1) {
		
	}
	
	class ChattMenuBar extends JMenuBar implements ActionListener , MouseListener{
		
		private static final long serialVersionUID = -4881020993046639013L;
		///
		private String menuName[] = {"파일","내정보","help"};
		private String fileMenu[] = {"창닫기"};	
		private String myMenu[] = {"대화명변경","친구"};
		private String mySubMenu[]={"추가","삭제","그룹이동"};
		private String helpmenu[]={"about"};
		
		public ChattMenuBar(){
			this.setBackground(SystemColor.control);
			initAWTContainer();
		}
		private void initAWTContainer(){
			JMenu menu = null;
			for (int i = 0; i < menuName.length; i++) {
				menu = new JMenu(menuName[i]);
				if(menuName[i].equals("파일")){
					createSubMenu(menu, fileMenu);
					menu.addActionListener(this);
					this.add(menu);
				}else if(menuName[i].equals("내정보")){
					createSubMenu(menu, myMenu);
					menu.addActionListener(this);
					this.add(menu);
				}else if(menuName[i].equals("help")){
					createSubMenu(menu, helpmenu);
					menu.addActionListener(this);
					this.add(menu);
				}
			}
		}
		
		private void createSubMenu(JMenu menu ,String[] data){
			for (int j = 0; j < data.length; j++) {
				if(data[j].equals("친구")){
					JMenu jMenu = new JMenu(data[j]);
					//menuInfo.put(data[j], jMenu);
					createSubMenu(jMenu, mySubMenu);//리커션
					jMenu.addActionListener(this);
					menu.add(jMenu);
				}else{
					JMenuItem item = new JMenuItem(data[j]);
					//menuInfo.put(data[j], item);
					item.addActionListener(this);
					menu.add(item);
				}
			}
		}
		public void actionPerformed(ActionEvent e) {
			String menuName = e.getActionCommand();
			if(menuName.equals("로그아웃")){
				System.out.println("로그아웃");
			}
		}
		public void mouseReleased(MouseEvent e) {
			
		}
		public void mouseClicked(MouseEvent e) {
			
		}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		public void mousePressed(MouseEvent e) {}
	}
	public static void main(String[] ar){
		new ChattingDialog();
	}
}
