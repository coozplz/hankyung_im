package uclick.messenger.client.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.swing.AbstractListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;

import uclick.messenger.client.MyMessengerInfo;
import uclick.messenger.client.ProtocolRequestEventHandler;
import uclick.messenger.client.protocol.MessageProtocol;
import uclick.messenger.client.protocol.Protocol;


public class FriendIvitedDialog extends JDialog implements ActionListener{

	  /**
	 * 
	 */
	  private static final long serialVersionUID = 128830090637168304L;
	  private static final Insets EMPTY_INSETS = new Insets(0, 0, 0, 0);
	  private static final String ADD_BUTTON_LABEL = ">>";
	  private static final String REMOVE_BUTTON_LABEL = "<<";
	  private static final String DEFAULT_SOURCE_CHOICE_LABEL = "초대 가능목록";
	  private static final String DEFAULT_DEST_CHOICE_LABEL = "초대한 목록";
	  private JLabel sourceLabel;
	  private JList sourceList;
	  private JList destList;
	  private JLabel destLabel;
	  private JButton addButton;
	  private JButton removeButton;
	  private JButton invitedBtn;
	  private JButton cancleBtn;
	
	  private JPanel jPanel1,jPanel2,jPanel3,jPanel4,jPanel5;
	  private ChattingDialog parent;
	  private SortedListModel destListModel;
	  private SortedListModel sourceListModel;
	  private MyMessengerInfo myInfo = MyMessengerInfo.getInstance();
	  private ProtocolRequestEventHandler eventHandler = 
		  										ProtocolRequestEventHandler.getEventHandler();
	  public FriendIvitedDialog(ChattingDialog parent , String title ,String[] obj) {
		  super(parent,title);
		  this.parent = parent;
		  initScreen();
	  }
	  private void initScreen() {
		//this.setModalityType(ModalityType.);
		this.setSize(400, 300);
		this.setVisible(true);
		this.setResizable(false);
	    Dimension parentSize = parent.getSize(); 
	    Point p = parent.getLocation(); 
	    this.setLocation(p.x + parentSize.width / 4, p.y + parentSize.height / 4);
	    setLayout(new BorderLayout());
	    jPanel1 = new JPanel();
	    jPanel2 = new JPanel();
	    jPanel3 = new JPanel();
	    jPanel4 = new JPanel();
	    jPanel5 = new JPanel();
	    jPanel1.setLayout(new GridBagLayout());
	    sourceLabel = new JLabel(DEFAULT_SOURCE_CHOICE_LABEL);
	    sourceListModel = new SortedListModel();
	    sourceList = new JList(sourceListModel);
	    //sourceListModel.addAll(obj);
	    sourceList.setCellRenderer(new FriendListRenderer());
	    jPanel1.add(sourceLabel, new GridBagConstraints(0, 0, 1, 1, 0, 0,
	        GridBagConstraints.CENTER, GridBagConstraints.NONE,
	        EMPTY_INSETS, 0, 0));
	    jPanel1.add(new JScrollPane(sourceList), new GridBagConstraints(0, 1, 1, 5, .5,
	        1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
	        EMPTY_INSETS, 0, 0));

	    addButton = new JButton(ADD_BUTTON_LABEL);
	    jPanel1.add(addButton, new GridBagConstraints(1, 2, 1, 2, 0, .25,
	        GridBagConstraints.CENTER, GridBagConstraints.NONE,
	        EMPTY_INSETS, 0, 0));
	    addButton.addActionListener(new AddListener());
	    removeButton = new JButton(REMOVE_BUTTON_LABEL);
	    jPanel1.add(removeButton, new GridBagConstraints(1, 4, 1, 2, 0, .25,
	        GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
	            0, 5, 0, 5), 0, 0));
	    removeButton.addActionListener(new RemoveListener());

	    destLabel = new JLabel(DEFAULT_DEST_CHOICE_LABEL);
	    destListModel = new SortedListModel();
	    destList = new JList(destListModel);
	    destList.setCellRenderer(new FriendListRenderer());
	    jPanel1.add(destLabel, new GridBagConstraints(2, 0, 1, 1, 0, 0,
	        GridBagConstraints.CENTER, GridBagConstraints.NONE,
	        EMPTY_INSETS, 0, 0));
	    jPanel1.add(new JScrollPane(destList), new GridBagConstraints(2, 1, 1, 5, .5,
	        1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
	        EMPTY_INSETS, 0, 0));
	    invitedBtn = new JButton("초대");
	 
	    cancleBtn = new JButton("취소");
	    jPanel2.add(invitedBtn);
	    jPanel2.add(cancleBtn);
	    this.add(jPanel1,BorderLayout.CENTER);
	    this.add(jPanel2,BorderLayout.SOUTH);
	    this.add(jPanel3,BorderLayout.NORTH);
	    this.add(jPanel4,BorderLayout.WEST);
	    this.add(jPanel5,BorderLayout.EAST);
	    addListener();
	    this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	    this.addWindowListener(new WindowAdapter() {
	    	
			public void windowClosing(WindowEvent e) {
				parent.addUserBtn.setEnabled(true);
				FriendIvitedDialog.this.dispose();
			}
		});
	 }
	 private void addListener(){
		 invitedBtn.addActionListener(this);
		 cancleBtn.addActionListener(this);
	 }
	 public void actionPerformed(ActionEvent e) {
		 Object obj = e.getSource();
		 if(obj==invitedBtn){
			 
			 System.out.println("초대버튼");
			 int size = destListModel.getSize();
			 String users[] = new String[size];
			 for (int i = 0; i < size; i++) {
				 String str = (String)destListModel.getElementAt(i);
				int dot = str.indexOf("(");
				users[i] =str.substring(0, dot);
				System.out.println(users[i]);
			}
			MessageProtocol protocol = new MessageProtocol();
			protocol.setProtocolData("protocol", Protocol.REQ_FRIEND_INVITED);
			protocol.setProtocolData("socket", parent.frame.getSocket());
			protocol.setProtocolData("roomnum", parent.roomNum);
			protocol.setProtocolData("myId", myInfo.getUser().getMemId());
			protocol.setProtocolData("friendUser", users);
			eventHandler.setProtocol(protocol);
			this.dispose();
		 }else if(obj==cancleBtn){
			 System.out.println("취소버튼");
			 this.dispose();
		 }
	 }
	 public String getSourceChoicesTitle() {
	    return sourceLabel.getText();
	 }

	 public void setSourceChoicesTitle(String newValue) {
	    sourceLabel.setText(newValue);
	 }

	 public String getDestinationChoicesTitle() {
	    return destLabel.getText();
	 }

	 public void setDestinationChoicesTitle(String newValue) {
	    destLabel.setText(newValue);
	 }

	 public void clearSourceListModel() {
	    sourceListModel.clear();
	 }

	 public void clearDestinationListModel() {
	    destListModel.clear();
	 }

	 public void addSourceElements(ListModel newValue) {
	    fillListModel(sourceListModel, newValue);
	 }

	 public void setSourceElements(ListModel newValue) {
	    clearSourceListModel();
	    addSourceElements(newValue);
	 }

	 public void addDestinationElements(ListModel newValue) {
	    fillListModel(destListModel, newValue);
	 }

	 private void fillListModel(SortedListModel model, ListModel newValues) {
	    int size = newValues.getSize();
	    for (int i = 0; i < size; i++) {
	      model.add(newValues.getElementAt(i));
	    }
	 }

	 public void addSourceElements(Object newValue[]) {
	    fillListModel(sourceListModel, newValue);
	 }

	 public void setSourceElements(Object newValue[]) {
	    clearSourceListModel();
	    addSourceElements(newValue);
	 }

	 public void addDestinationElements(Object newValue[]) {
	    fillListModel(destListModel, newValue);
	 }

	 private void fillListModel(SortedListModel model, Object newValues[]) {
	    model.addAll(newValues);
	 }

	 public Iterator<?> sourceIterator() {
	    return sourceListModel.iterator();
	 }

	 public Iterator<?> destinationIterator() {
	    return destListModel.iterator();
	 }

	 public void setSourceCellRenderer(ListCellRenderer newValue) {
	    sourceList.setCellRenderer(newValue);
	 }

	 public ListCellRenderer getSourceCellRenderer() {
	    return sourceList.getCellRenderer();
	 }

	 public void setDestinationCellRenderer(ListCellRenderer newValue) {
	    destList.setCellRenderer(newValue);
	 }

	 public ListCellRenderer getDestinationCellRenderer() {
	    return destList.getCellRenderer();
	 }

	 public void setVisibleRowCount(int newValue) {
	    sourceList.setVisibleRowCount(newValue);
	    destList.setVisibleRowCount(newValue);
	 }

	 public int getVisibleRowCount() {
	    return sourceList.getVisibleRowCount();
	 }

	 public void setSelectionBackground(Color newValue) {
	    sourceList.setSelectionBackground(newValue);
	    destList.setSelectionBackground(newValue);
	 }

	 public Color getSelectionBackground() {
	    return sourceList.getSelectionBackground();
	 }

	 public void setSelectionForeground(Color newValue) {
	    sourceList.setSelectionForeground(newValue);
	    destList.setSelectionForeground(newValue);
	 }

	 public Color getSelectionForeground() {
	    return sourceList.getSelectionForeground();
	 }

	 private void clearSourceSelected() {
	    Object selected[] = sourceList.getSelectedValues();
	    for (int i = selected.length - 1; i >= 0; --i) {
	      sourceListModel.removeElement(selected[i]);
	    }
	    sourceList.getSelectionModel().clearSelection();
	 }

	 private void clearDestinationSelected() {
	    Object selected[] = destList.getSelectedValues();
	    for (int i = selected.length - 1; i >= 0; --i) {
	      destListModel.removeElement(selected[i]);
	    }
	    destList.getSelectionModel().clearSelection();
	 }

  	private class AddListener implements ActionListener {
  	    public void actionPerformed(ActionEvent e) {
  	      Object selected[] = sourceList.getSelectedValues();
  	      addDestinationElements(selected);
  	      clearSourceSelected();
  	    }
  	 }

  	 private class RemoveListener implements ActionListener {
  	    public void actionPerformed(ActionEvent e) {
  	    	Object[] selected = destList.getSelectedValues();
  	      addSourceElements(selected);
  	      clearDestinationSelected();
  	    }
  	 }

  	class SortedListModel extends AbstractListModel {
  		
	  private static final long serialVersionUID = -2015288622214265472L;
	  SortedSet<Object> model;

  	  public SortedListModel() {
  	    model = new TreeSet<Object>();
  	  }
  	  public int getSize() {
  	    return model.size();
  	  }

  	  public Object getElementAt(int index) {
  	    return model.toArray()[index];
  	  }

  	  public void add(Object object) {
  	    if (model.add(object)) {
  	      fireContentsChanged(this, 0, getSize());
  	    }
  	  }

  	  public void addAll(Object elements[]) {
  	    Collection<Object> c = Arrays.asList(elements);
  	    model.addAll(c);
  	    fireContentsChanged(this, 0, getSize());
  	  }

  	  public void clear() {
  	    model.clear();
  	    fireContentsChanged(this, 0, getSize());
  	  }

  	  public boolean contains(Object element) {
  	    return model.contains(element);
  	  }

  	  public Object firstElement() {
  	    return model.first();
  	  }

  	  public Iterator<Object> iterator() {
  	    return model.iterator();
  	  }
  	  public Object lastElement() {
  	    return model.last();
  	  }

  	  public boolean removeElement(Object element) {
  	    boolean removed = model.remove(element);
  	    if (removed) {
  	      fireContentsChanged(this, 0, getSize());
  	    }
  	    return removed;
  	  }
  	}
	class FriendListRenderer extends JLabel implements ListCellRenderer {
		  /**
		 * 
		 */
		private static final long serialVersionUID = -3664962293360114193L;
		final Color HIGHLIGHT_COLOR = new Color(0, 0, 128);

		public FriendListRenderer() {
		    setOpaque(true);
		    setIconTextGap(5);
		}

		public Component getListCellRendererComponent(JList list, Object value,
		      int index, boolean isSelected, boolean cellHasFocus) {
		    String user = (String)value;
		    setText(user);
		    setIcon(new ImageIcon("image/online.gif"));
		    if (isSelected) {
		      setBackground(HIGHLIGHT_COLOR);
		      setForeground(Color.white);
		    } else {
		      setBackground(Color.white);
		      setForeground(Color.black);
		    }
		    return this;
		}
	}
}