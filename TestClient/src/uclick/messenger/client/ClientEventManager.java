package uclick.messenger.client;

import java.awt.BorderLayout;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Observable;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uclick.messenger.client.dto.FileDto;
import uclick.messenger.client.dto.MemberDto;
import uclick.messenger.client.file.Upload;
import uclick.messenger.client.group.GroupTreeManager;
import uclick.messenger.client.gui.ChattingDialog;
import uclick.messenger.client.gui.FriendIvitedDialog;
import uclick.messenger.client.gui.MainFrame;
import uclick.messenger.client.gui.MessageDialog;
import uclick.messenger.client.gui.MessageSelectUserDialog;
import uclick.messenger.client.gui.MyInfoPanel;
import uclick.messenger.client.gui.ReceiveMessageDialog;
import uclick.messenger.client.gui.UserTreePanel;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;


public class ClientEventManager extends Observable{
	
	private MainFrame frame;
	private MyInfoPanel userPanel;
	private UserTreePanel userTreePanel ;
	private ClientXMLConvertor convertor = new ClientXMLConvertor();
	private GroupTreeManager treeManager = GroupTreeManager.getInstance();
	private MyMessengerInfo myInfo = MyMessengerInfo.getInstance();
	private HashMap<String, ChattingDialog> roomData = new HashMap<String, ChattingDialog>();
	private String[] grouplist ={"가족","친구","직장","기타"};
	
	public ClientEventManager(MainFrame frame) {
		this.frame = frame;
	}
	public void createChattingFrame(Document document){
		String roomNum = convertor.getContent(document, "roomNum");
		String friendNick = convertor.getContent(document, "friendnick");
		ChattingDialog chattingDialog = new ChattingDialog(frame , myInfo , roomNum);
		chattingDialog.setTitle(friendNick+"님과 의 대화");
		roomData.put(roomNum, chattingDialog);
	}
	public void userLoginEvent(Document document){
		//treeManager = GroupTreeManager.getInstance();
		treeManager.createUserGroup(document, grouplist);
		userPanel = new MyInfoPanel();
		userTreePanel = new UserTreePanel(frame, treeManager);
		frame.initMenuSetting(true);
		frame.remove(frame.loginPanel);
		frame.repaint();
		frame.add(userPanel, BorderLayout.NORTH);
		frame.add(userTreePanel,BorderLayout.CENTER);
		userTreePanel.updateUI();
		frame.repaint();
		frame.getTrayIconTest().logmenuStatus(true);
		this.addObserver(frame.getTrayIconTest());
		this.setChanged();
		this.notifyObservers(myInfo.getUser());
		frame.clientFileServer = new ClientFileServer();
		
	}
	public void friendLoginEvent(Document document) {
		String friendId = convertor.getContent(document, "id");
		String friendName = convertor.getContent(document, "name");
		String friendNick = convertor.getContent(document, "nickname");
		String status = convertor.getContent(document, "status");
		
		DefaultMutableTreeNode userTree = userTreePanel.nodeTable.get(friendNick);
		MemberDto user = (MemberDto)userTree.getUserObject();
		user.setMemId(friendId);
		user.setName(friendName);
		user.setNickName(friendNick);
		user.setStatus(status);
		userTreePanel.updateUser(user);
		this.setChanged();
		this.notifyObservers(user);
	}
	
	public void reschatting(Document document) {
		String nick = convertor.getContent(document, "nick");
		String msg = convertor.getContent(document, "message");
		String roomNum = convertor.getContent(document, "roomnum");
		ChattingDialog chattingDialog = null;
		if(roomData.get(roomNum)!=null){
			chattingDialog = roomData.get(roomNum);
			chattingDialog.setVisible(true);
		}else{
			chattingDialog = new ChattingDialog(frame, myInfo, roomNum);
			chattingDialog.setTitle(nick+"님과의  대화");
			roomData.put(roomNum, chattingDialog);
		}
		chattingDialog.userChatt.append(nick+"님께서 하는말 :\n");
		chattingDialog.userChatt.append("   "+msg.trim()+"\n");
		chattingDialog.userChatt.setSelectionStart(chattingDialog.userChatt.getText().length());	
	}
	
	public void createFriendListDialog(Document document) {
		String roomNum= convertor.getContent(document, "roomnum");
		Vector<MemberDto> userList = treeManager.getGroupList(document, "user",false);
		String obj[] = new String[userList.size()];
		for (int i = 0; i < obj.length; i++) {
			MemberDto user = userList.elementAt(i);
			String userNick = user.getMemId()+"("+user.getNickName()+")";
			obj[i] = userNick;
		}
		ChattingDialog chattingDialog = roomData.get(roomNum);
		FriendIvitedDialog dialog = new FriendIvitedDialog(chattingDialog, "친구 리스트",obj);
		dialog.addSourceElements(obj);
	}
	/**
	 * 형진이 소스
	 */
	public void changeNick(Document document){
        
		String oldNick=convertor.getContent(document,"oldnickname");
		String newNick=convertor.getContent(document,"newnickname");
		String memId = convertor.getContent(document, "memid");
		
		if(memId.equals(myInfo.getUser().getMemId())){
			myInfo.setNickName(newNick);
			userTreePanel.updateUI();
		}else{
			userTreePanel.updateChangeNick(oldNick,newNick , memId);
		}
	}
	public void friendLogoutsucessEvent(Document document){
		String name = convertor.getContent(document, "name");
		String nickName=convertor.getContent(document,"nickname");
		String status = convertor.getContent(document, "status");
		userTreePanel.logoutUser(nickName);
		DefaultMutableTreeNode userTree = userTreePanel.nodeTable.get(nickName);
		MemberDto user = (MemberDto) userTree.getUserObject();
		user.setStatus(status);
		user.setName(name);
		this.setChanged();
		this.notifyObservers(user);
	}
	public void myLogoutSuccessEvent(Document document){
		String status = convertor.getContent(document, "status");
		frame.remove(userTreePanel);
		frame.remove(userPanel);
		if(!userTreePanel.nodeTable.isEmpty()){
			userTreePanel.nodeTable.clear();
		}
		MemberDto user = myInfo.getUser();
		user.setStatus(status);
		this.setChanged();
		this.notifyObservers(user);
		userTreePanel.userTree.removeAll();
		userTreePanel.treeModel.reload();
		frame.repaint();
		frame.add(frame.loginPanel, BorderLayout.CENTER);
		frame.initMenuSetting(false);
		frame.repaint();
		treeManager.getFriendGroupManager().removeAllElements();
		
	}
	public void createMsgFriendListDialog(Document document) {
		int msgNum = Integer.parseInt(convertor.getContent(document, "msgNum"));
		Vector<MemberDto> userList = treeManager.getGroupList(document, "user",false);
		String obj[] = new String[userList.size()];
		for (int i = 0; i < obj.length; i++) {
			MemberDto user = userList.elementAt(i);
			String userNick = user.getMemId()+"("+user.getNickName()+")";
			obj[i] = userNick;
		}
		MessageDialog dialog = (MessageDialog) frame.MessageMap.get(msgNum);
		MessageSelectUserDialog selectDialog = new MessageSelectUserDialog(dialog, "친구 리스트",obj);
		selectDialog.addSourceElements(obj);
	}
	/**
	 * 정수
	 */
	public void groupChangeEvent(Document document) {
		String friendId = convertor.getContent(document, "friendid");
		String friendNick = convertor.getContent(document, "friendnick");
		String groupName = convertor.getContent(document, "groupname");
		
		MemberDto oldUserData = (MemberDto)userTreePanel.nodeTable.get(friendNick).getUserObject();	
		MemberDto newUserData = new MemberDto();
		newUserData.setMemId(friendId);
		newUserData.setName(oldUserData.getName());
		newUserData.setNickName(friendNick);
		newUserData.setGroupName(groupName);
		newUserData.setStatus(oldUserData.getStatus());
		userTreePanel.updateGroup(oldUserData, newUserData);
	}
	
	public void messageReceiveEvent(Document document) {
		String id = convertor.getContent(document, "id");
		String name = convertor.getContent(document, "name");
		String nick = convertor.getContent(document, "nick");
		String message = convertor.getContent(document, "message");
		MemberDto user = new MemberDto();
		user.setMemId(id);
		user.setName(name);
		user.setNickName(nick);
		new ReceiveMessageDialog(frame,user,message);
	}
	public void addFriendFailEvent(Document document) {
		String friendid = convertor.getContent(document, "friendid");
		String msg = convertor.getContent(document, "msg");
		JOptionPane.showMessageDialog(frame, msg, "친구 추가 오류", JOptionPane.ERROR_MESSAGE);
	}
	
	public void addFriendSuccessEvent(Document document) {
		System.out.println("addFriendSuccessEvent 진입");
		String friendid = convertor.getContent(document, "friendid");
		String friendname = convertor.getContent(document, "friendname");
		String friendnick = convertor.getContent(document, "friendnick");
		//JOptionPane.showMessageDialog(frame, friendid+" 찾았지롱", "친구 추가 성공", JOptionPane.DEFAULT_OPTION);	
		
		MemberDto mDto = new MemberDto();
		mDto.setMemId(friendid);
		mDto.setName(friendname);
		mDto.setNickName(friendnick);
		mDto.setStatus("1");
		
		System.out.println("friendid : "+friendid);	
		userTreePanel.addFriend(mDto);

	}
	
	public void deleteFriendEvent(Document document) {
		System.out.println("deleteFriendEvent 진입");
		System.out.println("1");
		String friendid = convertor.getContent(document, "friendid");
		System.out.println("2");
		String friendnick = convertor.getContent(document, "friendnick");
		System.out.println(friendid+" : friendid");
		System.out.println(friendnick+" : friendnick");
		System.out.println("3");
		
		MemberDto mDto = new MemberDto();
		mDto.setMemId(friendid);
		mDto.setNickName(friendnick);
		System.out.println("4");
		userTreePanel.deleteFriend(mDto);
	}
public void prepareFileTransfer(Document document) {
		
//		<!ELEMENT messenger (protocol,receiver,filename,filesize,ip)>
		String pc = Protocol.FILE_TRANSFER_DATA+"";
		String receiver = convertor.getContent(document, "sender");
		String filename = convertor.getContent(document, "filename");
		String filesize = convertor.getContent(document, "filesize");
		String ip = "";
		try {
			ip = java.net.InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			
			e.printStackTrace();
		}
//		String ip = frame.clientFileServer.getServerip();

		/**
		 * 파일명을 만들기 (불필요한 경로명을 삭제한다.)
		 */
		String temp =filename;
		int index = temp.lastIndexOf("\\");		
		temp = temp.substring(index+1, temp.length());
		long size = Long.parseLong(filesize);
		
		FileDto fileDto = new FileDto();
		fileDto.setFilename(temp);
		fileDto.setFilesize(size);
		fileDto.setMember(receiver);
		
		frame.clientFileServer.setFileDto(fileDto);
//		<!ELEMENT messenger (protocol,receiver,filename,filesize,ip)>
		Document doc = convertor.getDoc();
		Element messengerElement = doc.createElement("messenger");
		doc.appendChild(messengerElement);
		convertor.createElement(doc, messengerElement, "protocol", pc);
		convertor.createElement(doc, messengerElement, "receiver", receiver);
		convertor.createElement(doc, messengerElement, "filename", filename);
		convertor.createElement(doc, messengerElement, "filesize", filesize);
		
		String message = receiver +"님이  " + temp+" ["+size /1024+"KB]"+"를 보내려고 합니다.";
		int n = JOptionPane.showConfirmDialog((JFrame)null, message, "파일전송", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		/**
		 * 사용자가 Yes를 하게 되면 serversocket의 ip를 설정하고 그렇지 않으면 -1을 반환한다.
		 */
		ip = (n == JOptionPane.YES_OPTION) ? ip : "-1";
		convertor.createElement(doc, messengerElement, "ip", ip);
		String resStr = convertor.textFromXML(doc, "xml/file/file_transfer_data.dtd");
		try {
			convertor.writer(resStr, frame.getSocket());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void fileTransferData(Document document) {
		
		String ip = convertor.getContent(document, "ip");
		String member = convertor.getContent(document, "receiver");
		String filename = convertor.getContent(document, "filename");
		long filesize = Long.parseLong((String)convertor.getContent(document, "filesize"));
		
		FileDto fileDto = new FileDto();
		fileDto.setFilename(filename);
		fileDto.setFilesize(filesize);
		fileDto.setMember(member);
		
		
		if(ip.equals("-1")){
			JOptionPane.showMessageDialog((JFrame)null , "상대방이 거절하였습니다.");
		}else{
			try {
				Socket socket = new Socket(ip,Protocol.CLIENT_PORT);
				System.out.println(socket.isConnected()+"???????????????????");
				new Upload(fileDto,socket);
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
