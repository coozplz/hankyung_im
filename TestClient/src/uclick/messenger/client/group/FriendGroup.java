package uclick.messenger.client.group;

import java.util.Iterator;
import java.util.Vector;

import uclick.messenger.client.dto.MemberDto;


public class FriendGroup {

	private String groupName;
	private Vector<MemberDto> memberList ;
	
	public FriendGroup(String groupName) {
		this.groupName = groupName;
		memberList = new Vector<MemberDto>();
	}
	public String getGroupName() {
		return groupName;
	}
	
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public void addUser(MemberDto dto){
		memberList.add(dto);
	}
	public MemberDto getMember(int index){
		return memberList.elementAt(index);
	}
	public void delUser(String id){
		Iterator<MemberDto> it = memberList.iterator();
		while (it.hasNext()) {
			MemberDto dto = (MemberDto) it.next();
			if(dto.getMemId().equals(id)){
				it.remove();
				break;
			}
		}
	}
	
	public MemberDto getMemberDto(int index){
		MemberDto dto = memberList.elementAt(index);
		return dto;
	}
	
	public void mathingFriendList(Vector<MemberDto> memberList){
		this.memberList.addAll(memberList);
	}
	
	public int getSize(){
		return this.memberList.size();
	}
	
	public Vector<MemberDto> getMemberList() {
		return memberList;
	}

	@Override
	public String toString() {
		return this.groupName;
	}
}
