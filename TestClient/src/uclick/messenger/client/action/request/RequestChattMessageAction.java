package uclick.messenger.client.action.request;

import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;


public class RequestChattMessageAction {

	public static void requestExcute(Protocol protocol) {
		try {
			sendMessage(protocol);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private static void sendMessage(Protocol protocol) throws Exception{
		
		ClientXMLConvertor convertor = new ClientXMLConvertor();
		Socket socket =(Socket)protocol.getContent("socket");
		String sProtocol = String.valueOf(protocol.getContent("protocol"));
		String roomNum = (String)protocol.getContent("roomnum");
		String id = (String)protocol.getContent("id");
		String nick = (String)protocol.getContent("nick");
		String message = (String)protocol.getContent("message");
		
        Document dom = convertor.getDocument("xml/general_chatt.xml");
        convertor.addContent(dom, "protocol", sProtocol);
        convertor.addContent(dom, "roomnum", roomNum);
        convertor.addContent(dom, "id", id);
        convertor.addContent(dom, "nick", nick);
        convertor.addContent(dom, "message", message);
    	String requestStr = convertor.textFromXML(dom ,"xml/general_chatt.dtd"); 
    	convertor.writer(requestStr, socket);
	}
}
