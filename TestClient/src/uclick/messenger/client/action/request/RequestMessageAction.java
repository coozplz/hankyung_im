package uclick.messenger.client.action.request;

import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.client.action.ProtocolAction;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;

public class RequestMessageAction implements ProtocolAction {

	@Override
	public void requestExcute(Protocol protocol) {
		ClientXMLConvertor convertor = new ClientXMLConvertor();
		String sProtocol = String.valueOf(protocol.getContent("protocol"));
		String memId =(String)protocol.getContent("id");
		String name =(String)protocol.getContent("name");
		String nick =(String)protocol.getContent("nick");
		String receiver[] = (String[])protocol.getContent("user");
		String message = (String)protocol.getContent("message");
		Socket socket = (Socket)protocol.getContent("socket");
		try {
			Document document = convertor.getDocument("xml/send_message.xml");
			convertor.addContent(document, "protocol", sProtocol);
			convertor.addContent(document, "id", memId);
			convertor.addContent(document, "name",name);
			convertor.addContent(document, "nick",nick);
			convertor.addContent(document, "message", message);
			convertor.addMajorContent(document, "userlist","user", receiver);
			String request = convertor.textFromXML(document, "xml/send_message.dtd");
			convertor.writer(request, socket);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
