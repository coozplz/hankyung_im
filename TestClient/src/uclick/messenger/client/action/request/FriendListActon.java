package uclick.messenger.client.action.request;

import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.client.action.ProtocolAction;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;


public class FriendListActon implements ProtocolAction {

	public void requestExcute(Protocol protocol) {
		ClientXMLConvertor convertor = new ClientXMLConvertor();
		int pc = (Integer)protocol.getContent("protocol");
		String id = (String)protocol.getContent("id");
		String roomNum = (String)protocol.getContent("roomnum");
		Socket socket = (Socket)protocol.getContent("socket");
		try{
			Document document = convertor.getDocument("xml/request_friend_list.xml");
			convertor.addContent(document, "protocol", String.valueOf(pc));
			convertor.addContent(document, "id",id);
			convertor.addContent(document, "roomnum",roomNum);
			String requestXML = convertor.textFromXML(document, "xml/request_friend_list.dtd");
			convertor.writer(requestXML, socket);
		}catch (Exception e) {
			e.printStackTrace();
		}	
	}
}
