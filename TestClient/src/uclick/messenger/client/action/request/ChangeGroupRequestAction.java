package uclick.messenger.client.action.request;

import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.client.action.AbstractRequestAction;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;



public class ChangeGroupRequestAction extends AbstractRequestAction{

	
	public synchronized void requestExcute(Protocol protocol) {
		
		ClientXMLConvertor convertor = new ClientXMLConvertor();	
		int pc = (Integer) protocol.getContent("protocol");
		Socket socket = (Socket) protocol.getContent("socket");
		String myId = (String)protocol.getContent("myId");
		String friendId = (String)protocol.getContent("friendId");
		String friendNick = (String)protocol.getContent("friendNick");
		String group_name = (String)protocol.getContent("group_name");
		Document doc = convertor.getDocument("xml/changegroup.xml");
		convertor.addContent(doc, "protocol", String.valueOf(pc));
		convertor.addContent(doc, "myid", myId);
		convertor.addContent(doc, "friendid", friendId);
		convertor.addContent(doc, "friendnick", friendNick);
		convertor.addContent(doc, "groupname", group_name);
		
		try {
			String requestStr = convertor.textFromXML(doc, "xml/changegroup.dtd");;
			convertor.writer(requestStr, socket);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
