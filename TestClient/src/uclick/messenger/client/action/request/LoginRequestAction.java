package uclick.messenger.client.action.request;

import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.client.action.AbstractRequestAction;
import uclick.messenger.client.dto.MemberDto;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;


public class LoginRequestAction extends AbstractRequestAction{

	
	public synchronized void requestExcute(Protocol protocol) {
		ClientXMLConvertor convertor = new ClientXMLConvertor();
		MemberDto dto = (MemberDto) protocol.getContent("member");
		String id = dto.getMemId();
		String password = dto.getPassword();
		int pc = (Integer) protocol.getContent("protocol");
		Socket socket = (Socket) protocol.getContent("socket");
		Document doc = convertor.getDocument("xml/login.xml");
		convertor.addContent(doc, "protocol", String.valueOf(pc));
		convertor.addContent(doc, "id", id);
		convertor.addContent(doc, "password", password);
		
		try {
			String requestStr = convertor.textFromXML(doc,"xml/login.dtd");
			System.out.println(requestStr);
			convertor.writer(requestStr, socket);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
