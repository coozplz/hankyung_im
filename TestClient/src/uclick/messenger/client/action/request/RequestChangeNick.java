package uclick.messenger.client.action.request;

import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.client.action.AbstractRequestAction;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;

public class RequestChangeNick extends AbstractRequestAction{


	public void requestExcute(Protocol protocol) {
		sendMessage(protocol);
	}

	public void sendMessage(Protocol protocol) {
		try {
			ClientXMLConvertor convertor=new ClientXMLConvertor();
			String myid=(String)protocol.getContent("myid");
			String oldnick=(String)protocol.getContent("oldnick");
			String newnick=(String)protocol.getContent("newnick");
			Socket socket=(Socket)protocol.getContent("socket");
			int pc=(Integer)protocol.getContent("protocol");
			Document document=convertor.getDocument("xml/changenick.xml");
	
			convertor.addContent(document,"protocol", String.valueOf(pc));//protocol을 저장 하고 
			convertor.addContent(document,"myid",myid);//아이디 담아 주고 
			convertor.addContent(document,"oldnick",oldnick);//별명을 담아 주고 
			convertor.addContent(document,"newnick",newnick);//별명을 담아 주고 
			String requestXml=convertor.textFromXML(document,"xml/changenick.dtd");
			convertor.writer(requestXml, socket);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
}
