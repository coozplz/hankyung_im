package uclick.messenger.client.action.request;

import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.client.action.AbstractRequestAction;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;


public class MakeRoomRequestAction extends AbstractRequestAction  {

	public synchronized void requestExcute(Protocol protocol) {
		ClientXMLConvertor convertor = new ClientXMLConvertor();
		
		String sProtocol = String.valueOf(protocol.getContent("protocol"));
		String myId = (String)protocol.getContent("myId");
		String friendId = (String)protocol.getContent("friendId");
		Socket socket = (Socket)protocol.getContent("socket");
		
		Document document = convertor.getDocument("xml/reqMakeRoom.xml");
		convertor.addContent(document, "protocol", sProtocol);
		convertor.addContent(document, "myId", myId);
		convertor.addContent(document, "friendId", friendId);
		
		try {
			String requestStr = convertor.textFromXML(document, "xml/reqMakeRoom.dtd");
			convertor.writer(requestStr, socket);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
}
