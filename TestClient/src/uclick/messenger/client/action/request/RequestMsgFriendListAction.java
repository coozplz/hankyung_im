package uclick.messenger.client.action.request;

import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.client.action.ProtocolAction;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;

public class RequestMsgFriendListAction implements ProtocolAction {

	public void requestExcute(Protocol protocol) {
		
		ClientXMLConvertor convertor = new ClientXMLConvertor();
		String sProtocol = String.valueOf(protocol.getContent("protocol"));
		String msgNum = String.valueOf(protocol.getContent("msgNum"));
		String id = (String)protocol.getContent("id");
		Socket socket = (Socket)protocol.getContent("socket");
		try{
			Document document = convertor.getDocument("xml/request_msg_friend_list.xml");
			convertor.addContent(document, "protocol", sProtocol);
			convertor.addContent(document, "msgNum", msgNum);
			convertor.addContent(document, "id", id);
			String requestXML = convertor.textFromXML(document, "xml/request_msg_friend_list.dtd");
			convertor.writer(requestXML, socket);
		}catch (Exception e) {
			e.printStackTrace();
		}	
	}
}
