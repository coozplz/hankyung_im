package uclick.messenger.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import com.ibm.mediumserver.Queue;
import com.ibm.mediumserver.TPThreadToo;

public class Server {
	
	private ServerSocket serverSocket;
	private Socket socket;
	private ServerRequestReciver serverThread;
	private static int portNum = 7777;
	private Queue requestQue;
	private Queue runnableQue;
	public Server() {
		setupNet();
	}
	private  void setupNet(){
	
		try {
			System.out.println("server initialize.....");
			System.out.println("server starting......server port 7777");
			serverSocket = new ServerSocket(portNum,10);
			serverSocket.setReuseAddress(true);
			requestQue = new Queue(10, 10);
			runnableQue = new Queue(10, 10,ServerRequestReciver.class);
			for (int i = 0; i < 10; i++) {
				Runnable runner = new ServerRequestReciver();
				runnableQue.add(runner);
			}
			for (int i = 0; i < 50; i++) {
				Thread thread = new TPThreadToo(requestQue, runnableQue);
				thread.start();
			}
		} catch (Exception e) {
			
			try {
				serverSocket.close();
			} catch (IOException e1) {
				
			}
			
			
		}		
	}
	public synchronized void run(){
		
		while (true) {
			try {
				
				socket = serverSocket.accept();
				socket.setReuseAddress(true);
				System.out.println(socket.getInetAddress().getHostAddress()+" 들어옴");
				serverThread = (ServerRequestReciver) runnableQue.next();
				serverThread.init(socket);
				requestQue.add(serverThread);
				
			} catch (IOException e) {
				
				break;
				
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void close(){
		try {
			System.out.println("server resource destroyed.....");
			
			if(socket!=null){
				socket.close();
			}
			if(serverSocket!=null){
				serverSocket.close();
			}
		} catch (IOException e) {
			
		}
	}
	public ServerSocket getServerSocket() {
		return serverSocket;
	}
	public Socket getSocket() {
		return socket;
	}
	public static void main(String[] ar){
		new Server().run();
	}
}
