package uclick.messenger.server.xml;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.Socket;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import uclick.messenger.server.ProjectUtil;


public class ServerXMLConvertor {
	/**
	 * Logger for this class
	 */
	private DocumentBuilderFactory factory;
	private DocumentBuilder builder;
	private TransformerFactory transformerFactory;
	private Transformer transformer;
	public static final int MAX_BUFFER_SIZE = 1024;
	private volatile int currentBuffer;

	public ServerXMLConvertor() {

		try {
			factory = DocumentBuilderFactory.newInstance();
			factory.setIgnoringElementContentWhitespace(true);
			builder = factory.newDocumentBuilder();
			transformerFactory = TransformerFactory.newInstance();
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		} catch (ParserConfigurationException e) {

			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			
			e.printStackTrace();
		}
	}

	public void createElement(Document document,Element parent , String tagName,String content){
		Element child = document.createElement(tagName);
		child.setTextContent(content);
		parent.appendChild(child);
	}
	public Document getDoc() {
		return builder.newDocument();
	}

	public synchronized String toXML(Document doc, String doctype) {
		try {
			transformerFactory = TransformerFactory.newInstance();
			transformer = transformerFactory.newTransformer();
			transformerFactory.setAttribute("indent-number", new Integer(4));
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype);
			transformer.setOutputProperty(OutputKeys.ENCODING, "euc-kr");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			
			Source source = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			Result result = new StreamResult(writer);
			transformer.transform(source, result);
			return writer.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public synchronized void responseWriter(String str, Socket socket) throws IOException {
		DataOutputStream dos = null;
		PrintWriter writer = null;
		StringReader strReader = null;
		
		dos = new DataOutputStream(socket.getOutputStream());
		writer = new PrintWriter(socket.getOutputStream());
		strReader = new StringReader(str.trim());
		int n = str.length();
//		dos.writeInt(n-2);

		dos.writeInt(n);
		synchronized (dos) {
			dos.flush();
		}	
		while (true) {
			char[] buffer = getBufferSize(n);
			int read = 0;
			
			read = strReader.read(buffer);
			if (read == -1 || currentBuffer >= n) {
				break;
			}
			writer.write(buffer, 0, read);
			currentBuffer += read;
		}
		writer.flush();
		currentBuffer = 0;
	}

	private synchronized char[] getBufferSize(int fileSize) {
		char buffer[] = new char[fileSize];
		return buffer;
	}

	public synchronized Document readXML(InputSource input)
			throws org.xml.sax.SAXException, IOException,
			ParserConfigurationException {
		if (input == null)
			throw new IOException("fromXML: null input");
		return builder.parse(input);
	}

	public Document getDocument(String fileName) {
		try {
			File f = new File(fileName);
			if (f.exists()) {
				return readXML(new InputSource(new FileReader(f)));
			} else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public synchronized void addContent(Document document, String tagName,
			String content) {
		Document doc = document;
		NodeList list = doc.getElementsByTagName(tagName);
		Element element = (Element) list.item(0);
		element.setTextContent(content);
	}

	public synchronized Document toDoc(String requestXML) throws SAXException,
			IOException {
		requestXML = ProjectUtil.deleteTrash(requestXML);
		StringReader reader = new StringReader(requestXML);
		InputSource source = new InputSource(reader);
		Document document = builder.parse(source);
		return document;
	}

	/**
	 * document type to string type
	 * 
	 * @param document
	 * @return
	 * @throws TransformerException
	 */
	public synchronized String toString(Document document)
			throws TransformerException {
		Source source = new DOMSource(document);
		StringWriter stringWriter = new StringWriter();
		Result result = new StreamResult(stringWriter);
		transformer.transform(source, result);
		return stringWriter.toString();
	}

	/**
	 * 
	 * @param document
	 * @param tagName
	 * @return
	 */
	public synchronized String getContent(Document document, String tagName) {
		NodeList list = document.getElementsByTagName(tagName);
		Element element = (Element) list.item(0);
		return element.getTextContent();
	}

	public synchronized Vector<String> getContentList(Document document,
			String tagName) {
		Vector<String> strList = new Vector<String>();
		NodeList list = document.getElementsByTagName(tagName);
		for (int i = 0; i < list.getLength(); i++) {
			Element element = (Element) list.item(i);
			strList.add(element.getTextContent());
		}
		return strList;
	}
	
	/**
	 * Document 유효성 체크 메소드(스키마..)
	 */
	public boolean docValidate(String requesetXML, String mathDocType) {
		boolean isValide = false;
		StringReader reader = new StringReader(requesetXML);
		Source xmlSource = new StreamSource(reader);
		Source[] dtdSource = new Source[] { new StreamSource(mathDocType) };
		isValide = XMLValidator.validateXMLSchema(xmlSource, dtdSource);
		return isValide;
	}
}
