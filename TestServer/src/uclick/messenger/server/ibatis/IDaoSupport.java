package uclick.messenger.server.ibatis;

import java.util.List;

public interface IDaoSupport {

	Object queryForObject(String sql , Object obj)throws Exception;
	List<?> queryFroList(String sql , Object obj)throws Exception;
	Object insert(String sql , Object obj)throws Exception;
	Object update(String sql , Object obj)throws Exception;
	Object delete(String sql,Object obj)throws Exception;
	
}
