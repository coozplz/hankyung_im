package uclick.messenger.server;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStreamReader;
import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.server.action.Action;
import uclick.messenger.server.action.ChattingAction;
import uclick.messenger.server.action.Protocol;
import uclick.messenger.server.action.ResponseActionFactory;
import uclick.messenger.server.xml.ServerXMLConvertor;


public class ServerRequestReciver implements Runnable {
	
	private static int visitorNumber = 0;
	private int myVisitorNumber;
	private Thread listener;
	private Socket socket;
	private ResponseActionFactory factory = ResponseActionFactory.getFactory();
	private ServerXMLConvertor convertor = new ServerXMLConvertor();
	private StringBuffer stringBuffer = new StringBuffer();
	private DataInputStream dis;
	private BufferedReader bin;
	
	public ServerRequestReciver(Socket socket) {
		this.socket = socket;
		synchronized (ServerRequestReciver.class) {
			visitorNumber++;
			myVisitorNumber = visitorNumber;
		}
	}

	public ServerRequestReciver() {
		
	}
	public void init(Socket socket) {
		synchronized (ServerRequestReciver.class) {
			visitorNumber++;
			myVisitorNumber = visitorNumber;
			System.out.println("current thread count : "+myVisitorNumber);
		}
		this.socket = socket;
	}
	public char[] getBufferSize(int size) {
		char[] buffer = new char[size];
		return buffer;
	}
	public synchronized void run() {
		try {
			listener = Thread.currentThread();
			bin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			dis = new DataInputStream(socket.getInputStream());
			System.out.println("server - run() ");
			String in = null;
			int read = 0;
			int xmlSize = 0;

			while (true) {
				xmlSize = dis.readInt();
				System.out.println("xml size:"+xmlSize);
				char[] buffer = getBufferSize(xmlSize);
				read = bin.read(buffer);
				if (read == -1) break;
				stringBuffer = stringBuffer.append(buffer, 0, read);
				System.out.println("xml string conv : "+stringBuffer.toString());
				if (xmlSize == stringBuffer.length()) {
					Document document = convertor.toDoc(stringBuffer.toString());
					if (getProtocol(document) == Protocol.GENERAL_MSG) {
						ChattingAction.excute(socket, document);
					} else {
						Action action = factory.getAction(document);
						action.excute(socket, document);
					}
					stringBuffer.delete(0, stringBuffer.length());
					stop();
				}
			}

		} catch (Exception e) {
			//new 
			e.printStackTrace();
		}
	}
	public synchronized void stop() {
		if (listener != null) {

			try {
				if (listener != Thread.currentThread())
					listener.interrupt();
					listener = null;
			} catch (Exception e) {

			}
		}
	}
	public synchronized int getProtocol(Document document) {
		return Integer.parseInt(convertor.getContent(document, "protocol"));
	}
}
