package uclick.messenger.server.action;

import java.net.Socket;
import java.util.Vector;

import org.w3c.dom.Document;

import uclick.messenger.server.chatting.MessangerUserInfo;
import uclick.messenger.server.dao.DaoManager;
import uclick.messenger.server.dto.MemberDto;
import uclick.messenger.server.xml.ServerXMLConvertor;

public class AddFriend implements Action{
	
	public void excute(Socket socket, Document document) {
		System.out.println("Server excute 진입");
		DaoManager manager  = DaoManager.getDaoManager();
		MessangerUserInfo userInfo =MessangerUserInfo.getUserInfo();
		ServerXMLConvertor convertor = new ServerXMLConvertor();
		
		String myid = convertor.getContent(document, "myid");
		String myname = convertor.getContent(document, "myname");
		String mynick = convertor.getContent(document, "mynick");
		String friendid = convertor.getContent(document, "friendid");
		
		if(myid.equals(friendid)){
			Document addFriendFail = convertor.getDocument("xml/addfriendfail.xml");
			convertor.addContent(addFriendFail, "protocol", String.valueOf(Protocol.RES_ADD_FRIEND_FAIL));
			convertor.addContent(addFriendFail, "friendid", friendid);
			convertor.addContent(addFriendFail, "msg", "자신은 친구가 될수 없습니다.");
			String res = convertor.toXML(addFriendFail, "xml/addfriendfail.dtd");
			try {
				convertor.responseWriter(res, socket);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			String isS = manager.getOneUser(myid.trim(), friendid.trim());
			
			if(isS==null){
				System.out.println("db작업성공~~");
				int count = manager.insertFriend(myid, friendid);
				if(count > 0){
					System.out.println("추가 성공");
					MemberDto friendDto = userInfo.findUser(friendid);
					System.out.println(friendDto.getNickName());
					
					//추가할친구..
					Document addFriendSuccess = convertor.getDocument("xml/addfriendsuccess.xml");
					convertor.addContent(addFriendSuccess, "protocol", String.valueOf(Protocol.RES_ADD_FRIEND_SUCCESS));
					convertor.addContent(addFriendSuccess, "friendid", myid);
					convertor.addContent(addFriendSuccess, "friendname", myname);
					convertor.addContent(addFriendSuccess, "friendnick", mynick);
					String res = convertor.toXML(addFriendSuccess, "xml/changegroupsuccess.dtd");
					try {
						convertor.responseWriter(res, friendDto.getSocket());
					} catch (Exception e) {
						e.printStackTrace();
					}
					System.out.println("친구한테 내 정보 날림성공");
					//내게로..
					convertor.addContent(addFriendSuccess, "protocol", String.valueOf(Protocol.RES_ADD_FRIEND_SUCCESS));
					convertor.addContent(addFriendSuccess, "friendid", friendDto.getMemId());
					convertor.addContent(addFriendSuccess, "friendname", friendDto.getName());
					convertor.addContent(addFriendSuccess, "friendnick", friendDto.getNickName());
					res = convertor.toXML(addFriendSuccess, "xml/changegroupsuccess.dtd");
					try {
						convertor.responseWriter(res, socket);
					} catch (Exception e) {
						e.printStackTrace();
					}
					System.out.println("내한테 친구 정보 날림성공");
					
				}else{
					
					System.out.println("insertFriend 문제발생");
					
				}
				
			}else{
				Document addFriendFail = convertor.getDocument("xml/addfriendfail.xml");
				convertor.addContent(addFriendFail, "protocol", String.valueOf(Protocol.RES_ADD_FRIEND_FAIL));
				convertor.addContent(addFriendFail, "friendid", friendid);
				convertor.addContent(addFriendFail, "msg", isS);
				String res = convertor.toXML(addFriendFail, "xml/addfriendfail.dtd");
				try {
					convertor.responseWriter(res, socket);
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("db작업실패!! : 그런사람없음");
				System.out.println("xml 날렸음");
			}
			
		}
		
	}

}
