package uclick.messenger.server.action;

import java.net.Socket;

import org.w3c.dom.Document;

public interface Action {

	void excute(Socket socket ,Document document);
}
