package uclick.messenger.server.action;

import java.net.Socket;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uclick.messenger.server.chatting.ChattingRoomInfo;
import uclick.messenger.server.chatting.ServerChattingManager;
import uclick.messenger.server.dao.DaoManager;
import uclick.messenger.server.dto.MemberDto;
import uclick.messenger.server.xml.ServerXMLConvertor;

public class FriendListResponseAction implements Action {

	public void excute(Socket socket, Document document) {
		Vector<MemberDto> list = new Vector<MemberDto>();
		Vector<MemberDto> roomUsers = new Vector<MemberDto>();
		ServerXMLConvertor convertor = new ServerXMLConvertor();
		DaoManager daoManager = DaoManager.getDaoManager();
		ServerChattingManager chattingManager = ServerChattingManager.getChattingManager();
		
		try {
			String id =convertor.getContent(document, "id");
			String roomNum =convertor.getContent(document, "roomnum");
			ChattingRoomInfo roomInfo = chattingManager.findChattingRoom(Integer.parseInt(roomNum));
			roomUsers.setSize(roomInfo.getUserList().size());
			Vector<MemberDto> onlineUserList = daoManager.getOnlineUserGroupList(id);
			for (int i = 0; i < onlineUserList.size(); i++) {
				MemberDto user = onlineUserList.elementAt(i);
				if(!roomInfo.findUser(user.getMemId())){
					list.add(user);
				}
			}
			Document resDocument =convertor.getDoc();
			Element emsg = resDocument.createElement("messenger");
			resDocument.appendChild(emsg);
			convertor.createElement(resDocument, emsg, "protocol", String.valueOf(Protocol.RES_FRIEND_LIST));
			convertor.createElement(resDocument, emsg, "roomnum", roomNum);
			Element eUserList = resDocument.createElement("userlist");
			emsg.appendChild(eUserList);
			Element user = null;
			for (int i = 0; i < list.size(); i++) {
				MemberDto member = list.elementAt(i);
				if(!member.getGroupName().equals("me")){
					user = resDocument.createElement("user");
					convertor.createElement(resDocument, user, "id", member.getMemId());
					convertor.createElement(resDocument, user, "name", member.getName());
					convertor.createElement(resDocument, user, "nickname", member.getNickName());
					eUserList.appendChild(user);
				}
			}
			String res = convertor.toXML(resDocument, "xml/res_friend_list.dtd");
			convertor.responseWriter(res, socket);	
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

}
