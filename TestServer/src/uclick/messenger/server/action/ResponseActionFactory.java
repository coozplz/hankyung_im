package uclick.messenger.server.action;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ResponseActionFactory {

	private static ResponseActionFactory factory;
	
	private ResponseActionFactory() {
	
	}
	
	public static ResponseActionFactory getFactory() {
		if(factory == null){
			factory = new ResponseActionFactory();
		}
		return factory;
	}

	public Action getAction(Document document){
		int protocol = getProtocol(document);
		Action action = null;
		if(protocol==Protocol.REQ_LOGIN){
			action= new LoginResponseAction();
		}else if(protocol==Protocol.REQ_MAKE_ROOM){
			action = new MakeRoomResponseAction();
		}else if(protocol==Protocol.REQ_LOG_OUT){
			action = new LogoutResponseAction();
		}else if(protocol==Protocol.REQ_FRIEND_LIST){
			action = new FriendListResponseAction();
		}else if(protocol==Protocol.REQ_FRIEND_INVITED){
			action = new FriendInvitedResponseAction();
		}else if(protocol==Protocol.REQ_CHANGE_NICK){
			action = new ChangeNickResponseAction();
		}else if(protocol==Protocol.REQ_MSG_FRIEND_LIST){
			action = new MsgFriendListResponseAction();
		}else if(protocol==Protocol.REQ_CHANGE_GROUP){
			action = new ChangeGroup();
		}else if(protocol==Protocol.SEND_MESSAGE){
			action = new ResponseMessageAction();
		}else if(protocol==Protocol.REQ_ADD_FRIEND){
			action = new AddFriend();
		}else if(protocol==Protocol.REQ_DELETE_FRIEND){
			action = new DeleteFriend();
		}else if(protocol == Protocol.FILE_TRANSFER_INIT){
			action = new RequestFileTransferInitAction();
		}else if(protocol == Protocol.FILE_TRANSFER_DATA){
			action = new ResponseFileTransferInitAction();
		}
		return action;
	}
	
	private int getProtocol(Document requestXML){
		NodeList list = requestXML.getElementsByTagName("protocol");
		Element element = (Element) list.item(0);
		return Integer.parseInt(element.getTextContent());
	}
}
