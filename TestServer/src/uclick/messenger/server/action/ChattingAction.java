package uclick.messenger.server.action;

import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.server.chatting.ChattingRoomInfo;
import uclick.messenger.server.chatting.ServerChattingManager;
import uclick.messenger.server.dto.MemberDto;
import uclick.messenger.server.xml.ServerXMLConvertor;


public class ChattingAction{

	public static synchronized void excute(Socket socket, Document document) {
		ServerChattingManager chattingManager =ServerChattingManager.getChattingManager();
		ServerXMLConvertor convertor =new ServerXMLConvertor();
		
		try {
			String sRoomNum = convertor.getContent(document, "roomnum");
			int roomNum = Integer.parseInt(sRoomNum);
			ChattingRoomInfo roomInfo = chattingManager.findChattingRoom(roomNum);
			int userSize = roomInfo.getUserList().size();
			for (int i = 0; i < userSize; i++) {
				MemberDto dto = roomInfo.getUserList().get(i);
				Socket soc = dto.getSocket();
				String res = convertor.toXML(document, "xml/general_chatt.dtd");
				convertor.responseWriter(res, soc);
			}
		} catch (Exception e) {
		
		}
	}
}
