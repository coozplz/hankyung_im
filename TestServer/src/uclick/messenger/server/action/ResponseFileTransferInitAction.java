package uclick.messenger.server.action;

import java.io.IOException;
import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.server.chatting.MessangerUserInfo;
import uclick.messenger.server.dto.MemberDto;
import uclick.messenger.server.xml.ServerXMLConvertor;

public class ResponseFileTransferInitAction implements Action {
	/**
	 * Logger for this class
	 */
	@Override
	public void excute(Socket socket, Document document) {
		
		ServerXMLConvertor convertor = new ServerXMLConvertor();
		MessangerUserInfo userInfo = MessangerUserInfo.getUserInfo();
		
		String sReceiver = convertor.getContent(document, "receiver");
		
		MemberDto receiver = userInfo.findUser(sReceiver) ;
//		<!ELEMENT messenger (protocol,sender,filename,filesize,roomnum,receiverlist)>
		String resXml = convertor.toXML(document, "xml/file/file_transfer_init.dtd");
		try {
			convertor.responseWriter(resXml, receiver.getSocket());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
