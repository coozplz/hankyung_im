package uclick.messenger.server.action;

import java.net.Socket;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uclick.messenger.server.chatting.MessangerUserInfo;
import uclick.messenger.server.dao.DaoManager;
import uclick.messenger.server.dto.MemberDto;
import uclick.messenger.server.xml.ServerXMLConvertor;


public class LoginResponseAction implements Action{
	
	public void excute(Socket socket, Document document) {
		
		Vector<String> friendNameList = new Vector<String>();
		MessangerUserInfo userInfo = MessangerUserInfo.getUserInfo();
		DaoManager manager  = DaoManager.getDaoManager();
		ServerXMLConvertor convertor = new ServerXMLConvertor();
		
		String id = convertor.getContent(document, "id");
		String password = convertor.getContent(document, "password");
		
		MemberDto dto = new MemberDto();
		dto.setMemId(id);
		dto.setPassword(password);
		dto = manager.loginUser(dto);
		if(!isNull(dto.getMemId())){
		
			dto.setSocket(socket);
			dto.setIp(socket.getInetAddress().getHostAddress());
			dto.setStatus("1");
			if(userInfo.findUsers(id)){
				System.out.println("old user find......");
				MemberDto oldUser = userInfo.findUser(id);
				Socket oldSoc = oldUser.getSocket();
				Document doc = convertor.getDocument("xml/logout.xml");
				convertor.addContent(doc, "protocol", String.valueOf(Protocol.REQ_LOG_OUT));
				convertor.addContent(doc, "id", oldUser.getMemId());
				
				Action action = new LogoutResponseAction();
				action.excute(oldSoc, doc);
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			userInfo.addUser(dto);
			manager.userStatusUpdate(dto.getMemId(), "1");
			
			Vector<MemberDto> userList = manager.getUserGroupList(dto.getMemId());
			Document resDocument =convertor.getDoc();
			Element emsg = resDocument.createElement("messenger");
			resDocument.appendChild(emsg);
			convertor.createElement(resDocument, emsg, "protocol", String.valueOf(Protocol.RES_LOGIN_SUCCESS));
			Element eUserList = resDocument.createElement("userlist");
			emsg.appendChild(eUserList);
			/**
			 * client's user info
			 */
			Element user = null;
			for (int i = 0; i < userList.size(); i++) {
				MemberDto member = userList.elementAt(i);
				if(member.getStatus().equals("1") && !member.getGroupName().equals("me")){
					friendNameList.add(member.getMemId());
				}
				user = resDocument.createElement("user");
				convertor.createElement(resDocument, user, "id", member.getMemId());
				convertor.createElement(resDocument, user, "name", member.getName());
				convertor.createElement(resDocument, user, "nickname", member.getNickName());
				//createElement(resDocument, user, "ip", member.getIp());
				convertor.createElement(resDocument, user, "status", member.getStatus());
				convertor.createElement(resDocument, user, "groupname", member.getGroupName());
				eUserList.appendChild(user);
			}
		
				try {
					String res = convertor.toXML(resDocument, "xml/loginsuccess.dtd");
					convertor.responseWriter(res, socket);
				} catch (Exception e1) {
				
					e1.printStackTrace();
				}
				
			////접속된 친구들에게 유저정보를 날린다.
			Document userLoginDoc = convertor.getDoc();
			Element eMsg = userLoginDoc.createElement("messenger");
			userLoginDoc.appendChild(eMsg);
			convertor.createElement(userLoginDoc, eMsg, "protocol", String.valueOf(Protocol.USER_LOGIN));
			convertor.createElement(userLoginDoc, eMsg, "id",dto.getMemId());
			convertor.createElement(userLoginDoc, eMsg, "name",dto.getName());
			convertor.createElement(userLoginDoc, eMsg, "nickname", dto.getNickName());
			convertor.createElement(userLoginDoc, eMsg, "status",dto.getStatus());
			try {
				for (int i = 0; i < friendNameList.size(); i++) {
					MemberDto friend = userInfo.findUser(friendNameList.elementAt(i));
					if(friend !=null){
						String resUser = convertor.toXML(userLoginDoc, "xml/user.dtd");
						System.out.println("여기에 오냐??");
						System.out.println(resUser);
						convertor.responseWriter(resUser, friend.getSocket());
					}
				}
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
		}else{
			try {
				Document doc = convertor.getDocument("xml/loginfail.xml");
				convertor.addContent(doc, "protocol", String.valueOf(Protocol.RES_LOGIN_FAIL));
				String res = convertor.toXML(doc, "xml/loginfail.dtd");
				convertor.responseWriter(res, socket);
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		}

		
	}
	private boolean isNull(String str){
		return str == null || str.trim().equals("");
	}

}
