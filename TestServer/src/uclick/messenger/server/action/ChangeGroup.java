package uclick.messenger.server.action;

import java.net.Socket;
import java.util.Vector;

import org.w3c.dom.Document;

import uclick.messenger.server.dao.DaoManager;
import uclick.messenger.server.dto.MemberDto;
import uclick.messenger.server.xml.ServerXMLConvertor;

public class ChangeGroup implements Action{

	Vector<String> buddyList = new Vector<String>();
	
	public void excute(Socket socket, Document document) {
		DaoManager manager  = DaoManager.getDaoManager();
		ServerXMLConvertor convertor = new ServerXMLConvertor();
		
		String mid = convertor.getContent(document, "myid");
		String fid = convertor.getContent(document, "friendid");
		String fnick = convertor.getContent(document, "friendnick");
		String group_name = convertor.getContent(document, "groupname");
		
		int count = manager.changeGroupUpdate(mid, fid, group_name);
		
		if(count==1){
					
			Document changeGroupDoc = convertor.getDocument("xml/changegroupsuccess.xml");
			convertor.addContent(changeGroupDoc, "protocol", String.valueOf(Protocol.RES_CHANGE_GROUP_SUCCESS));
			convertor.addContent(changeGroupDoc, "friendid", fid);
			convertor.addContent(changeGroupDoc, "friendnick", fnick);
			convertor.addContent(changeGroupDoc, "groupname", group_name);
			String res = convertor.toXML(changeGroupDoc, "xml/changegroupsuccess.dtd");
			try {
				convertor.responseWriter(res, socket);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			System.out.println("db작업실패!!");
		}
		
	}

}
