package uclick.messenger.server.action;

import java.net.Socket;
import java.util.Vector;

import org.w3c.dom.Document;

import uclick.messenger.server.chatting.MessangerUserInfo;
import uclick.messenger.server.dao.DaoManager;
import uclick.messenger.server.dto.MemberDto;
import uclick.messenger.server.xml.ServerXMLConvertor;

public class ChangeNickResponseAction implements Action{


	public void excute(Socket socket, Document document) {
		try{
			Vector<MemberDto>friendlist=new Vector<MemberDto>();
			MessangerUserInfo userInfo = MessangerUserInfo.getUserInfo();
			DaoManager dao=DaoManager.getDaoManager();//다오를 뽑아 오고 
			MemberDto dto=new MemberDto();
			ServerXMLConvertor convertor = new ServerXMLConvertor();
			String id=convertor.getContent(document,"myid");//바꾼 사람 아이디
			String oldnick=convertor.getContent(document,"oldnick");
			String newnick=convertor.getContent(document,"newnick");//새로운 아이디
	       
			dto.setMemId(id);
			dto.setNickName(newnick);
			dto.setSocket(socket);
			dto.setIp(socket.getInetAddress().getHostAddress());
			int result=dao.upDateNick(dto);
			if(result>0){
				if(userInfo.findUsers(id)){
					userInfo.removeUser(id);
					userInfo.addUser(dto);
				}
				
				Vector<MemberDto>list=dao.getOnlineUserGroupList(id);
				for (int i = 0; i < list.size(); i++) {
					MemberDto user = list.elementAt(i);
					if(userInfo.findUsers(user.getMemId())){
						friendlist.add(userInfo.findUser(user.getMemId()));
					}
				}	
				Document doc=convertor.getDocument("xml/reschangenick.xml");
				convertor.addContent(doc,"protocol",String.valueOf(Protocol.RES_CHANGE_NICK));
				convertor.addContent(doc,"memid",id);
				convertor.addContent(doc,"oldnickname",oldnick);
				convertor.addContent(doc,"newnickname",newnick);
				String res = convertor.toXML(doc,"xml/reschangenick.dtd");

				for(int i=0;i<friendlist.size();i++){   
					MemberDto user = friendlist.elementAt(i);
					Socket soc = user.getSocket();
					convertor.responseWriter(res, soc);
				}

			}else{
				System.out.println("update loose");
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}



