package uclick.messenger.server.action;

import java.net.Socket;
import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uclick.messenger.server.chatting.ServerChattingManager;
import uclick.messenger.server.dao.DaoManager;
import uclick.messenger.server.dto.MemberDto;
import uclick.messenger.server.xml.ServerXMLConvertor;

public class MsgFriendListResponseAction implements Action {

	@Override
	public void excute(Socket socket, Document document) {
		ServerXMLConvertor convertor = new ServerXMLConvertor();
		DaoManager daoManager = DaoManager.getDaoManager();
		ServerChattingManager chattingManager = ServerChattingManager.getChattingManager();
		
		try {
			String id =convertor.getContent(document, "id");
			String msgNum =convertor.getContent(document, "msgNum");
			Vector<MemberDto> onlineUserList = daoManager.getOnlineUserGroupList(id);
			Document resDocument =convertor.getDoc();
			Element emsg = resDocument.createElement("messenger");
			resDocument.appendChild(emsg);
			convertor.createElement(resDocument, emsg, "protocol", String.valueOf(Protocol.RES_MSG_FRIEND_LIST));
			convertor.createElement(resDocument, emsg, "msgNum", msgNum);
			
			Element eUserList = resDocument.createElement("userlist");
			emsg.appendChild(eUserList);
			Element user = null;
			for (int i = 0; i < onlineUserList.size(); i++) {
				MemberDto member = onlineUserList.elementAt(i);
				if(!member.getGroupName().equals("me")){
					user = resDocument.createElement("user");
					convertor.createElement(resDocument, user, "id", member.getMemId());
					convertor.createElement(resDocument, user, "name", member.getName());
					convertor.createElement(resDocument, user, "nickname", member.getNickName());
					eUserList.appendChild(user);
				}
			}
			String res = convertor.toXML(resDocument, "xml/res_msg_friend_list.dtd");
			convertor.responseWriter(res, socket);	
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
}
