package uclick.messenger.server.action;

import java.net.Socket;
import java.util.Iterator;
import java.util.Vector;

import org.w3c.dom.Document;

import uclick.messenger.server.chatting.ChattingRoomInfo;
import uclick.messenger.server.chatting.MessangerUserInfo;
import uclick.messenger.server.chatting.ServerChattingManager;
import uclick.messenger.server.dto.MemberDto;
import uclick.messenger.server.xml.ServerXMLConvertor;

public class FriendInvitedResponseAction implements Action {

	ServerChattingManager chattingManager = ServerChattingManager.getChattingManager();
	public void excute(Socket socket, Document document) {
		System.out.println("초대 액션에 왔음...");
		ServerXMLConvertor convertor = new ServerXMLConvertor();
		MessangerUserInfo userInfo = MessangerUserInfo.getUserInfo();
		try{
			String roomNum = convertor.getContent(document, "roomnum");
			Vector<String> Ids = convertor.getContentList(document, "friendid");
			Vector<MemberDto> userList = new Vector<MemberDto>();
			
			Iterator<String> iter = Ids.iterator();
			while (iter.hasNext()) {
				String id = (String) iter.next();
				MemberDto user = userInfo.findUser(id);
				userList.add(user);
			}
			invitedUserChattRoom(userList, Integer.parseInt(roomNum));
		}catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	private void invitedUserChattRoom(Vector<MemberDto> userList,int roomNum){
		ChattingRoomInfo roomInfo = chattingManager.findChattingRoom(roomNum);
		for (int i = 0; i < userList.size(); i++) {
			MemberDto user = userList.elementAt(i);
			roomInfo.addUser(user);
		}
	}
}
