package uclick.messenger.server.action;

import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.server.chatting.ChattingRoomInfo;
import uclick.messenger.server.chatting.MessangerUserInfo;
import uclick.messenger.server.chatting.ServerChattingManager;
import uclick.messenger.server.dto.MemberDto;
import uclick.messenger.server.xml.ServerXMLConvertor;


public class MakeRoomResponseAction implements Action {

	public void excute(Socket socket, Document document) {

		ServerXMLConvertor convertor =  new ServerXMLConvertor();
		//
		ServerChattingManager chattingManager =ServerChattingManager.getChattingManager();
		MessangerUserInfo userInfo = MessangerUserInfo.getUserInfo();
		//
		String myId = convertor.getContent(document, "myId");
		String friendId = convertor.getContent(document, "friendId");
		try {
			MemberDto user1 = userInfo.findUser(myId);
			
			MemberDto user2 = userInfo.findUser(friendId);
			
			ChattingRoomInfo chattingRoom = new ChattingRoomInfo();
			chattingRoom.initChattingRoom(user1, user2);
			chattingManager.addChattingRoom(chattingRoom);
			System.out.println("chatting room num : "+chattingRoom.getRoomNum());
			System.out.println("인원 : "+chattingRoom.getUserList().size());
			Document resDocument = convertor.getDocument("xml/resMakeRoom.xml");
			convertor.addContent(resDocument,"protocol", String.valueOf(Protocol.RES_MAKE_ROOM));
			convertor.addContent(resDocument, "roomNum", String.valueOf(chattingRoom.getRoomNum()));
			convertor.addContent(resDocument, "myId", user1.getMemId());
			convertor.addContent(resDocument, "friendnick", user2.getNickName());
			String res = convertor.toXML(resDocument, "xml/resMakeRoom.dtd");
			convertor.responseWriter(res, socket);
		} catch (Exception e) {
			
		}
	}
}
