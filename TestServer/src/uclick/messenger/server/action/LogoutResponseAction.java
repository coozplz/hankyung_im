package uclick.messenger.server.action;

import java.net.Socket;
import java.util.Vector;

import org.w3c.dom.Document;

import uclick.messenger.server.chatting.MessangerUserInfo;
import uclick.messenger.server.chatting.ServerChattingManager;
import uclick.messenger.server.dao.DaoManager;
import uclick.messenger.server.dto.MemberDto;
import uclick.messenger.server.xml.ServerXMLConvertor;



public class LogoutResponseAction implements Action {

	public void excute(Socket socket, Document documents) {
		Vector<MemberDto>friendlist=new Vector<MemberDto>();//친구 리스트를 담는것 
	
		System.out.println("logout action");
		ServerXMLConvertor convertor = new ServerXMLConvertor();
		DaoManager manager  = DaoManager.getDaoManager();
		MessangerUserInfo userInfo = MessangerUserInfo.getUserInfo();
		ServerChattingManager chattingManager = ServerChattingManager.getChattingManager();
		try {
			String id = convertor.getContent(documents, "id");
			int result = manager.userStatusUpdate(id, "0");//내 정보를 0으로 빠꿔 주고
			String nickname=manager.getNickName(id);
			if(result == 1){
		
				Document mydoc=convertor.getDocument("xml/mylogoutsuccess.xml");
			    convertor.addContent(mydoc, "protocol", String.valueOf(Protocol.MY_LOG_OUT_SUCCESS));
				convertor.addContent(mydoc,"id",id);
				convertor.addContent(mydoc,"nickname",nickname);
				convertor.addContent(mydoc,"status","0");
			    MemberDto mydto=userInfo.findUser(id);
				String myres=convertor.toXML(mydoc,"xml/mylogoutsuccess.dtd");
				convertor.responseWriter(myres,mydto.getSocket());
				//나한테 보내는 정보 
				//나 한테 보내야 되니깐 //내 정보를 챃자 
				Vector<MemberDto>list=manager.getOnlineUserGroupList(id);//해당 하는 친구의 정보를 가지고 와서
				for(int i=0;i<list.size();i++){
				    MemberDto user=list.get(i);
				    if(!user.getGroupName().equals("me")){
				    	if(userInfo.findUsers(user.getMemId())){
				    		friendlist.add(userInfo.findUser(user.getMemId()));
				    	}
				    }
				}
				Document document=convertor.getDocument("xml/logoutsuccess.xml");
				convertor.addContent(document, "protocol", String.valueOf(Protocol.RES_LOG_OUT_SUCCESS));
				convertor.addContent(document,"id",id);
				convertor.addContent(document,"name",mydto.getName());
				convertor.addContent(document,"nickname",nickname);
				convertor.addContent(document,"status","0");
				String res=convertor.toXML(document,"xml/logoutsuccess.dtd");
				chattingManager.removeChattingUser(id);
				userInfo.removeUser(id);//서버에 값을 지워 주고 
				
				for(int i=0;i<friendlist.size();i++){
					   MemberDto user=friendlist.get(i);
					   Socket soc=user.getSocket();
					   convertor.responseWriter(res, soc);	
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
