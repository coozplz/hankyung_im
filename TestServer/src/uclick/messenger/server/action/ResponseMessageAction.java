package uclick.messenger.server.action;

import java.net.Socket;
import java.util.Vector;

import org.w3c.dom.Document;

import uclick.messenger.server.chatting.MessangerUserInfo;
import uclick.messenger.server.dto.MemberDto;
import uclick.messenger.server.xml.ServerXMLConvertor;

public class ResponseMessageAction implements Action {

	public void excute(Socket socket, Document document) {
		
		ServerXMLConvertor convertor = new ServerXMLConvertor();
		MessangerUserInfo userInfo = MessangerUserInfo.getUserInfo();
		try {
			Vector<String> userlist=convertor.getContentList(document, "user");
			
			for (int i = 0; i < userlist.size(); i++) {
				String id = userlist.elementAt(i);
				if(userInfo.findUsers(id)){
					MemberDto receiver = userInfo.findUser(id);
					Socket soc = receiver.getSocket();
					String response = convertor.toXML(document, "xml/send_message.dtd");
					convertor.responseWriter(response, soc);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
