package uclick.messenger.server.action;

import java.net.Socket;
import java.util.Vector;

import org.w3c.dom.Document;

import uclick.messenger.server.chatting.MessangerUserInfo;
import uclick.messenger.server.dao.DaoManager;
import uclick.messenger.server.dto.MemberDto;
import uclick.messenger.server.xml.ServerXMLConvertor;

public class DeleteFriend implements Action{
	
	public void excute(Socket socket, Document document) {
		System.out.println("Server excute 진입");
		DaoManager manager  = DaoManager.getDaoManager();
		MessangerUserInfo userInfo =MessangerUserInfo.getUserInfo();
		ServerXMLConvertor convertor = new ServerXMLConvertor();
		
		String myid = convertor.getContent(document, "myid");
		String friendid = convertor.getContent(document, "friendid");
		String friendnick = convertor.getContent(document, "friendnick");
		
		int count = manager.deleteFriend(myid, friendid);
		
		if(count>0){
			MemberDto myDto = userInfo.findUser(myid);
			MemberDto friendDto = userInfo.findUser(friendid);
			//내게로 삭제 성공 알림
			Document deleteFriendSuccess = convertor.getDocument("xml/deletefriend.xml");
			convertor.addContent(deleteFriendSuccess, "protocol", String.valueOf(Protocol.RES_DELETE_FRIEND_SUCCESS));
			convertor.addContent(deleteFriendSuccess, "friendid", friendid);
			convertor.addContent(deleteFriendSuccess, "friendnick", friendnick);
			String res = convertor.toXML(deleteFriendSuccess, "xml/deletefriend.dtd");
			
			try {
				convertor.responseWriter(res, socket);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if(friendDto!=null){//삭제친구 접속해있다면
				convertor.addContent(deleteFriendSuccess, "protocol", String.valueOf(Protocol.RES_DELETE_FRIEND_SUCCESS));
				convertor.addContent(deleteFriendSuccess, "friendid", myDto.getMemId());
				convertor.addContent(deleteFriendSuccess, "friendnick", myDto.getNickName());
				res = convertor.toXML(deleteFriendSuccess, "xml/deletefriend.dtd");
				
				try {
					convertor.responseWriter(res, friendDto.getSocket());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}else{
				System.out.println("친구접속 X");
			}
			
		}else{
			System.out.println("DELETE처리시 문제가 발생하였음");
		}
		
	}

}
