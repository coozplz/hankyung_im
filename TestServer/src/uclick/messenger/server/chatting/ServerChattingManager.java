package uclick.messenger.server.chatting;

import java.util.HashMap;
import java.util.Map;

import uclick.messenger.server.dto.MemberDto;


public class ServerChattingManager {
	
	private static ServerChattingManager chattingManager;
	private static int roomNum;
	private Map<Integer, ChattingRoomInfo> roomData;
	
	private ServerChattingManager() {
		roomData =new HashMap<Integer, ChattingRoomInfo>(); 
	}
	
	public static ServerChattingManager getChattingManager() {
		if(chattingManager == null){
			chattingManager = new ServerChattingManager();
		}
		return chattingManager;
	}
	
	public synchronized void addChattingRoom( ChattingRoomInfo roomInfo){
		int num = roomNum++;
		System.out.println(num+"'s chatting room open");
		roomData.put(num, roomInfo);
		roomInfo.setRoomNum(num);
		System.out.println("current room size : "+roomData.size());
	}
	public synchronized ChattingRoomInfo findChattingRoom(int roomNum){
		return roomData.get(roomNum);
	}
	public synchronized void removeChattingRoom(int roomNum){
		System.out.println("remove chatting room");
		roomData.remove(roomNum);
		System.out.println("current room size : "+roomData.size());
	}
	public synchronized void removeChattingUser(String id){
		MemberDto user = null;
		int userSize = 0;
		for (int i = 0; i < roomNum; i++) {
			ChattingRoomInfo roomInfo = findChattingRoom(i);
			userSize = roomInfo.getUserList().size();
			for (int j = 0; j < userSize; j++) {
				user = roomInfo.getUserList().elementAt(j);
				if(user.getMemId().equals(id)){
					System.out.println("찾았다..");
					roomInfo.removeUser(user);break;
				}
			}
		}
	}
	public synchronized Map<Integer, ChattingRoomInfo> getRoomData() {
		return roomData;
	}
}
