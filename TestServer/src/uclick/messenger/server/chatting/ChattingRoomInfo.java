package uclick.messenger.server.chatting;

import java.util.Iterator;
import java.util.Vector;

import uclick.messenger.server.dto.MemberDto;


public class ChattingRoomInfo {

	private Vector<MemberDto> userList;
	private int roomNum;
	
	public ChattingRoomInfo() {
		userList = new Vector<MemberDto>();
	}
	public ChattingRoomInfo(Vector<MemberDto> userList, int roomNum) {
		
		this.userList = userList;
		this.roomNum = roomNum;
	}
	
	public synchronized void initChattingRoom(MemberDto user1 , MemberDto user2){
		userList.add(user1);
		userList.add(user2);
	}
	
	public synchronized Vector<MemberDto> getUserList() {
		return userList;
	}
	public synchronized boolean findUser(String id){
		Iterator<MemberDto> iter = userList.iterator();
		while (iter.hasNext()) {
			MemberDto memberDto = (MemberDto) iter.next();
			 if(memberDto.getMemId().equals(id)){
				 return true;
			 }
		}
		return false;
	}
	public synchronized void addUser(MemberDto dto){
		System.out.println(roomNum+"번 방 "+dto.getMemId()+"add");
		userList.add(dto);
	}
	public synchronized void removeUser(MemberDto dto){
		Iterator<MemberDto> iter = userList.iterator();
		int i = 0;
		while (iter.hasNext()) {
			MemberDto memberDto = (MemberDto) iter.next();
			 if(memberDto.getIp().equals(dto.getIp())){
				 userList.remove(i);
				 break;
			 }
			 i++;
		}
	}
	public synchronized int getRoomNum() {
		return roomNum;
	}
	public synchronized void setRoomNum(int roomNum) {
		this.roomNum = roomNum;
	}
}
