package uclick.messenger.server.chatting;

import java.util.Iterator;
import java.util.Vector;

import uclick.messenger.server.dto.MemberDto;


public class MessangerUserInfo {
	/**
	 * 
	 */
	private static MessangerUserInfo userInfo ;
	
	private Vector<MemberDto> user;

	private MessangerUserInfo() {
		user = new Vector<MemberDto>(5,5);
	}
	
	public static MessangerUserInfo getUserInfo() {
		if(userInfo == null){
			userInfo = new MessangerUserInfo();
		}
		return userInfo;
	}

	public synchronized void addUser(MemberDto dto){
		System.out.println(dto.getName()+"'s user info input");
		user.add(dto);
	}
	/**
	 * remove userinfo from user ip
	 * @param ip
	 */
	public synchronized void removeUser(String id){
		for (int i = 0; i < user.size(); i++) {
			MemberDto dto = user.elementAt(i);
			if(dto.getMemId().equals(id)){
				user.remove(i);
			}
		}
	}
	/**
	 * 
	 * @param ip
	 * @return
	 */
	public synchronized MemberDto findUser(String id){
		//MemberDto dto = null;
		Iterator<MemberDto> iter = user.iterator();
		while (iter.hasNext()) {
			MemberDto dto = (MemberDto) iter.next();
			if(dto.getMemId().equals(id)){
				System.out.println("유저찾음");
				return dto;
			}
		}
		return null;
	}
	public synchronized boolean findUsers(String id){
		//MemberDto dto = null;
		Iterator<MemberDto> iter = user.iterator();
		while (iter.hasNext()) {
			MemberDto dto = (MemberDto) iter.next();
			if(dto.getMemId().equals(id)){
				System.out.println("유저찾음");
				return true;
			}
		}
		return false;
	}
	public synchronized Vector<MemberDto> findInitUser(String[] ip){
		Vector<MemberDto> userList = new Vector<MemberDto>();
		MemberDto dto = null;
		int i = 0;
		Iterator<MemberDto> iter = user.iterator();
		while (iter.hasNext()) {
			dto = (MemberDto) iter.next();
			if(dto.getIp().equals(ip[i])){
				if(i==2)break;
				userList.add(dto);
				i++;
			}
		}
		return userList;
	}
	/**
	 * 
	 * @return
	 */
	public Vector<MemberDto> getUser() {
		return user;
	}
}
