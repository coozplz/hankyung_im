package uclick.messenger.server.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import uclick.messenger.server.dto.MemberDto;


public class DaoManager {

	private static DaoManager daoManager;

	private DaoManager() {
		init();
	}

	public static DaoManager getDaoManager() {
		if(daoManager==null){
			daoManager = new DaoManager();
		}
		return daoManager;
	}
	private void init(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
//			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * @return
	 * @throws SQLException
	 * @throws ConnException 
	 */
	private Connection getConnection() throws SQLException{
		Connection conn=null;
		String url="jdbc:mysql://localhost:3306/hankyung";
		String user="root";
		String passwd="cafe2413";
		conn=DriverManager.getConnection(url,user,passwd);
		return conn;
	}//
	private void close(Connection conn, Statement psmt, ResultSet rs){
		if(rs!=null){
			try {
				rs.close();
			} catch (SQLException e) {
			}
		}
		if(psmt!=null){
			try {
				psmt.close();
			} catch (SQLException e) {
			}
		}
		if(conn!=null){
			try {
				conn.close();
			} catch (SQLException e) {
			}
		}
	}//
	/**
	 * 형진이 소스
	 * @param dto
	 * @return
	 */
	public int upDateNick(MemberDto dto)
	{
		Connection con=null;
		PreparedStatement psmt=null;
		int result=0;
        String sql="update member_info set nickname=? where m_id=?";
        try {
			con=getConnection();
			psmt=con.prepareStatement(sql);
			psmt.setString(1,dto.getNickName());
			psmt.setString(2,dto.getMemId());
			result=psmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			
			close(con, psmt,null);
		}
		return result;
	}
	/**
	 * 
	 * @param dto
	 * @return
	 */
	public synchronized MemberDto loginUser(MemberDto dto){
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		MemberDto member = new MemberDto();
		String sql = " SELECT id, password, name, nickname, status, auth" +
					 " FROM member m, member_info mi " +
					 " WHERE m.id = mi.m_id" +
					 " AND id = ? " +
					 " AND password = ? ";
		try {
			conn = getConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, dto.getMemId());
			psmt.setString(2, dto.getPassword());
			rs = psmt.executeQuery();
			while(rs.next()){
				int i =1;
				member.setMemId(rs.getString(i++));
				member.setPassword(rs.getString(i++));
				member.setName(rs.getString(i++));
				member.setNickName(rs.getString(i++));
				member.setStatus(rs.getString(i++));
//				member.setAuth(Integer.parseInt(rs.getString(i++)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			close(conn, psmt, rs);
		}
		return member;
	}
	public synchronized Vector<MemberDto> getUserGroupList(String id){
		Vector<MemberDto> userList = new Vector<MemberDto>();
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		String sql =" SELECT id, " + 
					"        password, " + 
					"        name, " + 
					"        nickname, " + 
					"        status, " + 
					"        auth, " + 
					"        NVL(group_name,'me') " + 
					" FROM ( " + 
					"         SELECT * " + 
					"         FROM member " + 
					"         WHERE id IN ( " + 
					"                       SELECT fri_id " + 
					"                       FROM member_group " + 
					"                       WHERE m_id = ? " + 
					"                     ) OR id = ?             " + 
					"       ) m, " + 
					"       (  " + 
					"         SELECT * " + 
					"         FROM member_group " + 
					"         WHERE m_id = ? " + 
					"       ) g, " + 
					"       member_info mi " + 
					" WHERE m.id = g.fri_id(+) " + 
					" AND m.id = mi.m_id ";
		System.out.println(sql);
		try {
			conn = getConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, id);
			psmt.setString(2, id);
			psmt.setString(3, id);
			rs = psmt.executeQuery();
			while(rs.next()){
				MemberDto member = new MemberDto();
				int i =1;
				member.setMemId(rs.getString(i++));
				member.setPassword(rs.getString(i++));
				member.setName(rs.getString(i++));
				member.setNickName(rs.getString(i++));
				member.setStatus(rs.getString(i++));
				member.setAuth(Integer.parseInt(rs.getString(i++)));
				member.setGroupName(rs.getString(i++));
				userList.add(member);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			close(conn, psmt, rs);
		}
		
		return userList;
	}
	
	public synchronized int changeGroupUpdate(String mid , String fid, String group_name){
		Connection conn = null;
		PreparedStatement psmt = null;
		String sql = " UPDATE member_group " + 
					 " SET group_name = ? " + 
					 " WHERE m_id = ? and fri_id = ? ";
				
		try {
			conn = getConnection();
			psmt = conn.prepareStatement(sql);
			
			psmt.setString(1, group_name);
			psmt.setString(2, mid);
			psmt.setString(3, fid);
			psmt.executeUpdate();
			return 1;
		} catch (SQLException e) {
			
			e.printStackTrace();
			return 0;
		}finally{
			close(conn, psmt, null);
		}
	}
	
	public synchronized int userStatusUpdate(String id , String status){
		Connection conn = null;
		PreparedStatement psmt = null;
		String sql = " UPDATE member_info " + 
					 " SET status = ? " + 
					 " WHERE m_id = ? ";
		
		try {
			conn = getConnection();
			psmt = conn.prepareStatement(sql);
			
			psmt.setString(1, status);
			psmt.setString(2, id);
			psmt.executeUpdate();
			return 1;
		} catch (SQLException e) {
			
			e.printStackTrace();
			return 0;
		}finally{
			close(conn, psmt, null);
		}
	}

	public Vector<MemberDto> getOnlineUserGroupList(String id) {
		Vector<MemberDto> userList = new Vector<MemberDto>();
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		
		String sql =" SELECT id, " + 
					"        password, " + 
					"        name, " + 
					"        nickname, " + 
					"        status, " + 
					"        auth, " + 
					"        NVL(group_name,'me') " + 
					" FROM ( " + 
					"         SELECT * " + 
					"         FROM member " + 
					"         WHERE id IN ( " + 
					"                       SELECT fri_id " + 
					"                       FROM member_group " + 
					"                       WHERE m_id = ? " + 
					"                     ) OR id = ?             " + 
					"       ) m, " + 
					"       (  " + 
					"         SELECT * " + 
					"         FROM member_group " + 
					"         WHERE m_id = ? " + 
					"       ) g, " + 
					"       member_info mi " + 
					" WHERE m.id = g.fri_id(+) " + 
					" AND m.id = mi.m_id " +
					" AND mi.status='1'";
		try {
			conn = getConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, id);
			psmt.setString(2, id);
			psmt.setString(3, id);
			rs = psmt.executeQuery();
			while(rs.next()){
				MemberDto member = new MemberDto();
				int i =1;
				member.setMemId(rs.getString(i++));
				member.setPassword(rs.getString(i++));
				member.setName(rs.getString(i++));
				member.setNickName(rs.getString(i++));
				member.setStatus(rs.getString(i++));
				member.setAuth(Integer.parseInt(rs.getString(i++)));
				member.setGroupName(rs.getString(i++));
				userList.add(member);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			close(conn, psmt, rs);
		}
		
		return userList;
	}
	public 	String getNickName(String id)
	{
		Connection conn = null;
		PreparedStatement psmt = null;
		String nickname=null;
		ResultSet rs;
		String sql = " SELECT NICKNAME " + 
					 " FROM MEMBER_INFO " + 
					 " WHERE m_id = ? ";
		try {
			conn = getConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, id);
			rs=psmt.executeQuery();
            if(rs.next())
            {
            	nickname=rs.getString(1);
            	
            }
		} catch (SQLException e) {
			
			e.printStackTrace();

		}finally{
			close(conn, psmt, null);
		}
		return nickname;
	}
	public int insertFriend(String mid , String fid){
		Connection conn = null;
		PreparedStatement psmt = null;
		String sql = " INSERT INTO member_group " + 
					 " values(?,?,'기타')";
		try {
			conn = getConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, mid);
			psmt.setString(2, fid);
			psmt.executeUpdate();
			
			System.out.println("1차 성공");
			
			psmt.setString(1, fid);
			psmt.setString(2, mid);
			psmt.executeUpdate();
			
			System.out.println("2차 성공");
			return 1;
		} catch (SQLException e) {
			
			e.printStackTrace();
			return 0;
		}finally{
			close(conn, psmt, null);
		}
	}

	public int deleteFriend(String myid, String friendid) {
		Connection conn = null;
		PreparedStatement psmt = null;
		String sql = " DELETE FROM member_group " +  
					 " WHERE m_id = ? AND fri_id = ? ";
		
		try {
			conn = getConnection();
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, myid);
			psmt.setString(2, friendid);
			psmt.executeUpdate();
			
			psmt.setString(1, friendid);
			psmt.setString(2, myid);
			psmt.executeUpdate();
			return 1;
		} catch (SQLException e) {
			
			e.printStackTrace();
			return 0;
		}finally{
			close(conn, psmt, null);
		}
	}
	public String getOneUser(String mid, String fid){
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs;
		String msg = null;
		
		//회원확인
		String sql_1 = " SELECT id " + 
					   " FROM member " + 
					   " WHERE id = ? ";
		//친구확인
		String sql_2 = " SELECT m_id " + 
					   " FROM member_group " + 
					   " WHERE m_id = ? AND fri_id = ? ";
		//온라인확인
		String sql_3 = " SELECT id, name, nickname " + 
					 " FROM member m, member_info mi " + 
					 " WHERE m.id = mi.m_id" +
					 " AND id = ? AND status='1'";
		try {
			
			conn = getConnection();
			psmt = conn.prepareStatement(sql_1);
			psmt.setString(1, fid);
			rs=psmt.executeQuery();
			if(!rs.next()){
            	return msg="해당 아이디가 존재하지 않습니다.";            	
            }
			
			psmt = conn.prepareStatement(sql_2);
			psmt.setString(1, mid);
			psmt.setString(2, fid);
			rs=psmt.executeQuery();
			if(rs.next()){
            	return msg="이미 등록된 친구입니다.";            	
            }
			
			psmt = conn.prepareStatement(sql_3);
			psmt.setString(1, fid);
			rs=psmt.executeQuery();
			int i=1;
            if(rs.next()){
            	return msg;            	
            }else{
            	return msg="검색한 회원이 로그아웃상태입니다.";
            }
            
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			close(conn, psmt, null);
		}
		return msg;
	}
}
