package uclick.messenger.server.dao.manager;

import java.util.List;

import uclick.messenger.server.dto.MemberDto;

public interface IDaoManager {

	MemberDto loginUser(String sql , MemberDto dto);
	int updateUserState(String sql , MemberDto dto);
	List<MemberDto> getAllUserGroupList(String sql , String id);
}
