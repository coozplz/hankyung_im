package uclick.messenger.server.dto;

import java.io.Serializable;

public class GroupInformation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1040202419885863966L;
	private int seq;
	private String memId;
	private String friendId;
	private String groupName;
	
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public String getMemId() {
		return memId;
	}
	public void setMemId(String memId) {
		this.memId = memId;
	}
	public String getFriendId() {
		return friendId;
	}
	public void setFriendId(String friendId) {
		this.friendId = friendId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	
}
