package uclick.messenger.client.action;


import uclick.messenger.client.action.request.AddFriendRequestAction;
import uclick.messenger.client.action.request.ChangeGroupRequestAction;
import uclick.messenger.client.action.request.DeleteFriendRequestAction;
import uclick.messenger.client.action.request.FriendInvitedRequestAction;
import uclick.messenger.client.action.request.FriendListActon;
import uclick.messenger.client.action.request.LoginRequestAction;
import uclick.messenger.client.action.request.LogoutRequestAction;
import uclick.messenger.client.action.request.MakeRoomRequestAction;
import uclick.messenger.client.action.request.RequestChangeNick;
import uclick.messenger.client.action.request.RequestFileSendInitAction;
import uclick.messenger.client.action.request.RequestMessageAction;
import uclick.messenger.client.action.request.RequestMsgFriendListAction;
import uclick.messenger.client.protocol.Protocol;



public class ProtocolRequestFactory {

	private static ProtocolRequestFactory eventActionFactory;
	
	private ProtocolRequestFactory(){
		
	}

	public static ProtocolRequestFactory getEventActionFactory() {
		if(eventActionFactory == null){
			eventActionFactory = new ProtocolRequestFactory();
		}
		return eventActionFactory;
	}
	
	public ProtocolAction getEventAction(Protocol protocol){
		
		ProtocolAction request = null;
		int pc = getProtocol(protocol);
		if(pc==Protocol.REQ_LOGIN){
			request = new LoginRequestAction();
		}else if(pc==Protocol.REQ_MAKE_ROOM){
			request = new MakeRoomRequestAction();
		}else if(pc==Protocol.REQ_LOG_OUT){
			request = new LogoutRequestAction();
		}else if(pc==Protocol.REQ_FRIEND_LIST){
			request = new FriendListActon();
		}else if(pc==Protocol.REQ_FRIEND_INVITED){
			request = new FriendInvitedRequestAction();
		}else if(pc==Protocol.REQ_CHANGE_NICK){
			request = new RequestChangeNick();
		}else if(pc==Protocol.REQ_MSG_FRIEND_LIST){
			request = new RequestMsgFriendListAction();
		}else if(pc==Protocol.SEND_MESSAGE){
			request = new RequestMessageAction();
		}else if(getProtocol(protocol)==Protocol.REQ_CHANGE_GROUP){
			request = new ChangeGroupRequestAction();
		}else if(getProtocol(protocol)==Protocol.REQ_ADD_FRIEND){
			request = new AddFriendRequestAction();
		}else if(getProtocol(protocol)==Protocol.REQ_DELETE_FRIEND){
			request = new DeleteFriendRequestAction();
		}else if(pc== Protocol.FILE_TRANSFER_INIT){
			request = new RequestFileSendInitAction();
		}
		return request;
	}
	
	private int getProtocol(Protocol protocol){
		return (Integer)protocol.getContent("protocol");
	}
}
