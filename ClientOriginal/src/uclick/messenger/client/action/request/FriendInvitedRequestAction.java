package uclick.messenger.client.action.request;

import java.net.Socket;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import uclick.messenger.client.action.ProtocolAction;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;

public class FriendInvitedRequestAction implements ProtocolAction {

	public void requestExcute(Protocol protocol) {
		ClientXMLConvertor  convertor = new ClientXMLConvertor();
		
		String pc = String.valueOf(protocol.getContent("protocol"));
		Socket socket = (Socket)protocol.getContent("socket");
		String roomNum = (String)protocol.getContent("roomnum");
		String myId = (String)protocol.getContent("myId");
		Object users[] = (Object[])protocol.getContent("friendUser");
		try{
			Document document =convertor.getDoc();
			Element emsg = document.createElement("messenger");
			document.appendChild(emsg);
			convertor.createElement(document, emsg, "protocol", pc);
			convertor.createElement(document, emsg, "myid", myId);
			convertor.createElement(document, emsg, "roomnum", roomNum);
			Element eUserList = document.createElement("userlist");
			emsg.appendChild(eUserList);
			for (int i = 0; i < users.length; i++) {
				convertor.createElement(document, eUserList, "friendid", (String)users[i]);
			}
			emsg.appendChild(eUserList);
			String reqStr = convertor.textFromXML(document, "xml/req_friend_invited.dtd");
			System.out.println("request : \n"+reqStr);
			convertor.writer(reqStr, socket);
		}catch (Exception e) {
			e.printStackTrace();
		}	
	}
}
