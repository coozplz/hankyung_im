package uclick.messenger.client.action.request;

import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.client.action.AbstractRequestAction;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;

public class AddFriendRequestAction extends AbstractRequestAction{


	public void requestExcute(Protocol protocol) {
		sendMessage(protocol);
	}

	private void sendMessage(Protocol protocol) {
		try {
			System.out.println("AddFriendRequestAction ����");
			ClientXMLConvertor convertor=new ClientXMLConvertor();
			String myid=(String)protocol.getContent("myid");
			String myname=(String)protocol.getContent("myname");
			String mynick=(String)protocol.getContent("mynick");
			String friendid=(String)protocol.getContent("friendid");
			Socket socket=(Socket)protocol.getContent("socket");
			int pc=(Integer)protocol.getContent("protocol");
			Document document=convertor.getDocument("xml/addfriend.xml");
	
			convertor.addContent(document,"protocol", String.valueOf(pc));
			convertor.addContent(document,"myid",myid); 
			convertor.addContent(document,"myname",myname); 
			convertor.addContent(document,"mynick",mynick);
			convertor.addContent(document,"friendid",friendid);
			String requestXml=convertor.textFromXML(document,"xml/addfriend.dtd");
			convertor.writer(requestXml, socket);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
}
