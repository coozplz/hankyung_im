package uclick.messenger.client.action.request;

import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.client.action.AbstractRequestAction;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;


public class LogoutRequestAction extends AbstractRequestAction {

	public synchronized void requestExcute(Protocol protocol) {
		ClientXMLConvertor convertor = new ClientXMLConvertor();
		Socket socket = (Socket) protocol.getContent("socket");
		String sProtocol = String.valueOf(protocol.getContent("protocol"));
		String id = (String)protocol.getContent("id");
		
		Document document = convertor.getDocument("xml/logout.xml");
		convertor.addContent(document, "protocol", sProtocol);
		convertor.addContent(document, "id", id);
		String requestStr = convertor.textFromXML(document,"xml/logout.dtd");
		try {
			convertor.writer(requestStr, socket);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
