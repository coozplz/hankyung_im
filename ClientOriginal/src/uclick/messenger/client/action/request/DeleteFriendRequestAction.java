package uclick.messenger.client.action.request;

import java.net.Socket;

import org.w3c.dom.Document;

import uclick.messenger.client.action.AbstractRequestAction;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;

public class DeleteFriendRequestAction extends AbstractRequestAction{


	public void requestExcute(Protocol protocol) {
		sendMessage(protocol);
	}

	public void sendMessage(Protocol protocol) {
		try {
			System.out.println("DeleteFriendRequestAction 진입");
			ClientXMLConvertor convertor=new ClientXMLConvertor();
			
			String myId=(String)protocol.getContent("myId");
			String friendId=(String)protocol.getContent("friendId");
			String friendNick=(String)protocol.getContent("friendNick");
			Socket socket=(Socket)protocol.getContent("socket");
			int pc=(Integer)protocol.getContent("protocol");
			
			Document document=convertor.getDocument("xml/deletefriend.xml");
			convertor.addContent(document,"protocol", String.valueOf(pc));//protocol을 저장 하고 
			convertor.addContent(document,"myid",myId); 
			convertor.addContent(document,"friendid",friendId);
			convertor.addContent(document,"friendnick",friendNick);
			String requestXml=convertor.textFromXML(document,"xml/deletefriend.dtd");
			convertor.writer(requestXml, socket);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
