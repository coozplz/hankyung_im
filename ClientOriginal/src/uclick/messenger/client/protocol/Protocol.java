package uclick.messenger.client.protocol;


public interface Protocol {

	static final int REQ_LOGIN = 101;
	static final int RES_LOGIN_SUCCESS = 102;
	static final int RES_LOGIN_FAIL = 103;
	static final int USER_LOGIN = 104;
	static final int REQ_LOG_OUT = 105;
	static final int RES_LOG_OUT_SUCCESS = 106;
	static final int MY_LOG_OUT_SUCCESS = 107;
	
	static final int REQ_MAKE_ROOM = 201;
	static final int RES_MAKE_ROOM = 202;
	static final int GENERAL_MSG = 203;
	static final int REQ_IVITED_FRIEND=204;
	static final int REQ_FRIEND_LIST=205;
	static final int RES_FRIEND_LIST=206;
	static final int REQ_FRIEND_INVITED = 207;
	
	//message
	static final int REQ_MSG_FRIEND_LIST=208;
	static final int RES_MSG_FRIEND_LIST=209;
	static final int SEND_MESSAGE = 210;
	///����
	static final int REQ_ADD_FRIEND = 301;
	static final int RES_ADD_FRIEND_FAIL = 302;
	static final int RES_ADD_FRIEND_SUCCESS = 303;
	static final int REQ_DELETE_FRIEND = 304;
	static final int RES_DELETE_FRIEND_SUCCESS = 305;
	
	static final int REQ_CHANGE_GROUP = 401;
	static final int RES_CHANGE_GROUP_SUCCESS = 402;
	
	static final int REQ_CHANGE_NICK=503;
	static final int RES_CHANGE_NICK=504;
	
	/*/////////////  File Transfer /////////////// */
	static final int FILE_TRANSFER_INIT = 601;
	static final int FILE_TRANSFER_DATA = 602;
	static final int CLIENT_PORT  = 7878;
	static final String DOWNLOAD_ADDRESS = System.getProperty("user.home")
	+ "\\UclickMessenger";
	static final int MAX_BUFFER_SIZE = 1024;
	
	/*///////////////////////////////////////////// */
	void setProtocolData(String entity,Object message);
	Object getContent(String key);
}
