package uclick.messenger.client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import javax.swing.JOptionPane;

import org.w3c.dom.Document;

import uclick.messenger.client.gui.MainFrame;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.xml.ClientXMLConvertor;


public class ClientResponseReceiver implements Runnable{

	private Socket socket;
	private MainFrame frame;
	private Thread listener;
	private BufferedReader bin;
	private DataInputStream dis;
	private ClientXMLConvertor convertor = new ClientXMLConvertor();
	private ClientEventManager eventManager;
	int temp;;
	int currentBuffer;
	public ClientResponseReceiver(Socket socket , MainFrame frame) {
		this.socket = socket;
		this.frame = frame;
		eventManager = new ClientEventManager(frame);
	}
	private synchronized char[] getBufferSize(int size) {
		char buffer[] = new char[size];
		return buffer;
	}
	public void run() {
		int read =0;
		try {
			StringBuffer strBuffer = new StringBuffer();
			
			while (true) {
				temp = dis.readInt();
				System.out.println("xml size :"+temp);
				char[] buffer = this.getBufferSize(temp);
				read = bin.read(buffer);
				if(read == -1){
					break;
				}
				System.out.println("CurrentBuffer :" + read);
				strBuffer.append(buffer,0,read);
				System.out.println("xml data: \n"+strBuffer.toString());
				if(temp==strBuffer.length()){
					Document document = convertor.toDoc(strBuffer.toString());
					int pc = Integer.parseInt(convertor.getContent(document, "protocol"));
					if(pc==Protocol.RES_LOGIN_SUCCESS){
						
						eventManager.userLoginEvent(document);
						
					}else if(pc==Protocol.RES_LOGIN_FAIL){
						
						JOptionPane.showMessageDialog(null, "로그인실패");
						
					}else if(pc==Protocol.USER_LOGIN){
						
						eventManager.friendLoginEvent(document);
						
					}else if(pc==Protocol.RES_LOG_OUT_SUCCESS){
						
						eventManager.friendLogoutsucessEvent(document);
						
					}else if(pc==Protocol.RES_MAKE_ROOM){
						
						eventManager.createChattingFrame(document);
						
					}else if(pc==Protocol.GENERAL_MSG){
						
						eventManager.reschatting(document);
						
					}else if(pc==Protocol.RES_FRIEND_LIST){
						
						eventManager.createFriendListDialog(document);
						
					}else if(pc==Protocol.RES_MSG_FRIEND_LIST){
						eventManager.createMsgFriendListDialog(document);
						
					}else if(pc==Protocol.RES_CHANGE_NICK){
						
						eventManager.changeNick(document);
						
					}else if(pc==Protocol.MY_LOG_OUT_SUCCESS){
					
						eventManager.myLogoutSuccessEvent(document);
						
					}else if(pc==Protocol.RES_CHANGE_GROUP_SUCCESS){
						
						eventManager.groupChangeEvent(document);
						
					}else if(pc==Protocol.SEND_MESSAGE){
						
						eventManager.messageReceiveEvent(document);
						
					}else if(pc==Protocol.RES_ADD_FRIEND_FAIL){
						
						eventManager.addFriendFailEvent(document);
						
					}else if(pc==Protocol.RES_ADD_FRIEND_SUCCESS){
						
						eventManager.addFriendSuccessEvent(document);
						
					}else if(pc==Protocol.RES_DELETE_FRIEND_SUCCESS){

						eventManager.deleteFriendEvent(document);
						
					}else if(pc == Protocol.FILE_TRANSFER_INIT){
						eventManager.prepareFileTransfer(document);
						
					} else if( pc == Protocol.FILE_TRANSFER_DATA){
						eventManager.fileTransferData(document);
					}
					strBuffer.delete(0, strBuffer.length());
				}
			}	
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frame, "일시적 네크워크 장애");
			e.printStackTrace();
		}finally{
			
		}
		stop();
	}
	public synchronized void start(){
		if(listener == null){
			try {
				bin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				dis = new DataInputStream(socket.getInputStream());
			} catch (IOException e) {
				try {
					socket.close();
				} catch (IOException e1) {
					e.printStackTrace();
				}
			}
			listener = new Thread(this);
			listener.start();
		}
	}
	public synchronized void stop(){
		if(listener != null){
			listener.interrupt();
			listener = null;
		}
	}
}
