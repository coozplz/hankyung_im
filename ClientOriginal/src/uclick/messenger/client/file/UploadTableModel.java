package uclick.messenger.client.file;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JProgressBar;
import javax.swing.table.AbstractTableModel;


class UploadsTableModel extends AbstractTableModel implements Observer {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6947286348032872182L;

	private static final String[] columnNames = { "받는 사람", "파일명", "크기(KB)",
			"진행", "상태" };

	private static final Class[] columnClasses = { String.class, String.class,
			String.class, JProgressBar.class, String.class };

	private ArrayList<Upload> uploadList = new ArrayList<Upload>();

	public void addUpload(Upload upload) {
		upload.addObserver(this);
		uploadList.add(upload);
		
		int n = uploadList.size();
		if(n >= 0 ){
			n = 1;
		}
		fireTableRowsInserted(getRowCount() - 1, getRowCount() - 1);
	}

	public Upload getUpload(int row) {
		Upload returnUpload = (Upload) uploadList.get(row);
		return returnUpload;
	}

	public void clearUpload(int row) {
		uploadList.remove(row);
		fireTableRowsDeleted(row, row);
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	public String getColumnName(int col) {
		String returnString = columnNames[col];
		return returnString;
	}

	@SuppressWarnings("unchecked")
	public Class getColumnClass(int col) {
		Class returnClass = columnClasses[col];
		return returnClass;
	}

	public int getRowCount() {
		int returnint = uploadList.size();
		return returnint;
	}

	public Object getValueAt(int row, int col) {
		Upload upload = uploadList.get(row);
		switch (col) {
		case 0:
			return upload.getReceiver();
		case 1: // URL
			return upload.getFileName();
		case 2: // Size
			int size = (int) upload.getTotalSize();
			int uploaded = upload.getUploaded();
			return (size == -1) ? "" : uploaded/1024 +" / "+ Integer.toString(size / 1024) + "KB";
		case 3: // Progress
			return new Float(upload.getProgress());
		case 4: // Status
			return Upload.UPSTATUSES[upload.getStatus()];
		}
		return "";
	}

	public void update(Observable o, Object arg) {
		int index = uploadList.indexOf(o);
		fireTableRowsUpdated(index, index);
	}
}
