package uclick.messenger.client.file;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


public class DownloadManager extends JFrame implements Observer, ActionListener {
	/**
	 * Logger for this class
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = -2956956175952852343L;
	protected DownloadsTableModel tableModel;

	protected JTable table = new JTable();
	private JButton pauseButton;
	private JButton resumeButton;
	private JButton cancelButton;
	private JButton clearButton;

	private Download selectedDownload;
	private boolean clearing;
	private static DownloadManager downloadManager;
	private DownloadManager(){
		init();
	}
	
	public static DownloadManager getInstance(){
		if(downloadManager == null){
			downloadManager = new DownloadManager();
		}
		return downloadManager;
	}
	public void init() {
		this.setTitle("다운로드 관리자");
		setSize(640, 300);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				actionExit();
			}
		});
		/* 다운로드 테이블 모델을 생성 */

		tableModel = new DownloadsTableModel();
		table = new JTable(tableModel);
		table.getSelectionModel().addListSelectionListener(
				new ListSelectionListener() {
					@Override
					public void valueChanged(ListSelectionEvent e) {
						tableSelectionChanged();
					}
				});

		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		ProgressRenderer renderer = new ProgressRenderer(0, 100);
		renderer.setStringPainted(true);
		table.setDefaultRenderer(JProgressBar.class, renderer);
		table.setRowHeight((int) (renderer.getPreferredSize().getHeight()));

		JPanel downloadPanel = new JPanel();
		downloadPanel.setBorder(BorderFactory.createTitledBorder("Downloads"));
		downloadPanel.setLayout(new BorderLayout());
		downloadPanel.add(new JScrollPane(table), BorderLayout.CENTER);

		// Buttons 설정
		JPanel buttonsPanel = new JPanel();
		pauseButton = new JButton("PAUSE");
		pauseButton.setEnabled(false);
		pauseButton.addActionListener(this);
		buttonsPanel.add(pauseButton);
		

		resumeButton = new JButton("RESUME");
		resumeButton.setEnabled(false);
		resumeButton.addActionListener(this);
		buttonsPanel.add(resumeButton);

		cancelButton = new JButton("CANCEL");
		cancelButton.setEnabled(false);
		cancelButton.addActionListener(this);
		buttonsPanel.add(cancelButton);

		clearButton = new JButton("clear");
		clearButton.setEnabled(false);
		clearButton.addActionListener(this);
		buttonsPanel.add(clearButton);

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(downloadPanel, BorderLayout.CENTER);
		getContentPane().add(buttonsPanel, BorderLayout.SOUTH);
		updateButtons();
	}

	protected void actionClear() {
		clearing = true;
		tableModel.clearDownload(table.getSelectedRow());
		clearing = false;
		selectedDownload = null;

	}

	protected void actionCancel() {
		selectedDownload.cancel();
		updateButtons();

	}

	protected void actionResume() {
		selectedDownload.resume();
		updateButtons();

	}

	protected void actionPause() {
		selectedDownload.pause();
		updateButtons();

	}

	protected void tableSelectionChanged() {
		if (selectedDownload != null) {
			selectedDownload.deleteObserver(DownloadManager.this);
		}
		if (!clearing) {
			selectedDownload = tableModel.getDownload(table.getSelectedRow());
			selectedDownload.addObserver(DownloadManager.this);
			updateButtons();
		}

	}

	protected void actionAdd(Download download) {

		tableModel.addDownload(download);
		updateButtons();
	}

	private void actionExit() {
		dispose();
	}

	@Override
	public void update(Observable o, Object arg) {
		if (selectedDownload != null && selectedDownload.equals(arg)) {
			updateButtons();

		}
	}

	protected void updateButtons() {
		if (selectedDownload != null) {
			int status = selectedDownload.getStatus();
			switch (status) {
			case Download.DOWNLOADING:
				pauseButton.setEnabled(true);
				resumeButton.setEnabled(false);
				cancelButton.setEnabled(true);
				clearButton.setEnabled(false);
				break;
			case Download.PAUSED:
				pauseButton.setEnabled(false);
				resumeButton.setEnabled(true);
				cancelButton.setEnabled(true);
				clearButton.setEnabled(false);
				break;
			case Download.ERROR:
				pauseButton.setEnabled(false);
				resumeButton.setEnabled(true);
				cancelButton.setEnabled(false);
				clearButton.setEnabled(true);
				break;
			default: //
				pauseButton.setEnabled(false);
				resumeButton.setEnabled(false);
				cancelButton.setEnabled(false);
				clearButton.setEnabled(true);
			}
		} else {
			pauseButton.setEnabled(false);
			resumeButton.setEnabled(false);
			cancelButton.setEnabled(false);
			clearButton.setEnabled(false);
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == pauseButton ) {
			actionPause();
		} else if (e.getSource()==resumeButton) {
			actionResume();
		} else if (e.getSource()==cancelButton) {
			actionCancel();
		} else if (e.getSource()==clearButton) {
			actionClear();
		}

	}
}
