package uclick.messenger.client.file;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.Socket;
import java.util.Observable;

import uclick.messenger.client.dto.FileDto;
import uclick.messenger.client.protocol.Protocol;

public class Download extends Observable implements Runnable {
	/**
	 * Logger for this class
	 */

	public static final String DWSTATUSES[] = { "DOWNLOADING", "PAUSED",
			"COMPLETE", "CANCELLED", "ERROR" };
	public long getDownloaded() {
		return downloaded;
	}

	public static final int DOWNLOADING = 0;
	public static final int PAUSED = 1;
	public static final int COMPLETE = 2;
	public static final int CANCELLED = 3;
	public static final int ERROR = 4;
	
	private Socket socket;
	private String sender;
	private String filename;

	private long size;
	private long downloaded = 0; // 다운 받은 파일의 크기
	private int status;

	private BufferedInputStream bis;
	private RandomAccessFile raf;
	private File downloadFile;


	DownloadManager downloadManager;

	public Download(FileDto fileDto, Socket socket) {
		downloadManager = DownloadManager.getInstance();
		this.socket = socket;
		/* ------------------------- */
		this.filename = fileDto.getFilename();
		this.size = fileDto.getFilesize();
		this.sender = fileDto.getMember();
		this.status = Download.DOWNLOADING;
		downloadManager.actionAdd(this);
		downloadManager.setVisible(true);
		downloadFile = new File(Protocol.DOWNLOAD_ADDRESS);
		if (!downloadFile.exists()) {
			downloadFile.mkdir();
		}
		downloadFile = new File(Protocol.DOWNLOAD_ADDRESS, this.getFileName());
		download();
		// 파일의

	}
	private void download() {
		Thread thread = new Thread(this);
		thread.start();
	}
	@Override
	public void run() {
		try {
			System.out.println("?????down?????????");
			raf = new RandomAccessFile(downloadFile, "rw");
			raf.seek(downloaded);
			bis = new BufferedInputStream(socket.getInputStream());
			int count = 1;
			while (status == Download.DOWNLOADING) {
				byte[] buffer = this.getBufferSize();
				int read = bis.read(buffer);
				if (read == -1 || downloaded >= size) {
					break;
				}
				raf.write(buffer, 0, read);
				downloaded += read;
				if(count%500 == 0 ){
					stateChanged();
				}
				count++;
			}
			if (status == Download.DOWNLOADING) {
				status = Download.COMPLETE;
				stateChanged();
			}
		} catch (FileNotFoundException e) {
			
			error();
		} catch (IOException e) {
			
			error();
		} finally {
			try {
				if (raf != null) {
					raf.close();
				}
				if (bis != null) {
					bis.close();
				}
				if(socket != null){
					socket.close();
				}

			} catch (IOException e) {
			

				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void stateChanged() {
		setChanged();
		notifyObservers();

	}

	private byte[] getBufferSize() {
		byte[] buffer;
		if (size - downloaded > Protocol.MAX_BUFFER_SIZE) {
			buffer = new byte[Protocol.MAX_BUFFER_SIZE];
		} else {
			buffer = new byte[(int) (size - downloaded)];
		}
		return buffer;
	}

	public String getFileName() {
		return filename;
	}

	public String getSender() {
		return sender;
	}

	public long getSize() {
		return size;
	}

	public int getStatus() {
		return status;
	}

	public float getProgress() {
		return ((float) downloaded / size) * 100;
	}

	public void pause() {
		status = Download.PAUSED;
		stateChanged();
	}

	public void cancel() {
		status = Download.CANCELLED;
		stateChanged();
	}

	public void error() {
		status = Download.ERROR;
		stateChanged();
	}

	public void resume() {
		status = Download.DOWNLOADING;
		stateChanged();
		download();
	}
}
