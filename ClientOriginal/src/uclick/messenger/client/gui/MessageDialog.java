package uclick.messenger.client.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import uclick.messenger.client.MyMessengerInfo;
import uclick.messenger.client.ProtocolRequestEventHandler;
import uclick.messenger.client.dto.MemberDto;
import uclick.messenger.client.protocol.MessageProtocol;
import uclick.messenger.client.protocol.Protocol;

public class MessageDialog extends JDialog implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -634540264177500982L;
	private Container container;
	private JPanel northCenterP,northWestP,northEastP,northNP;
	private JPanel northPanel;
	private JPanel centerPanel;
	private JPanel southPanel;
	private JScrollPane pane;
	public static JTextArea area;
	private JTextField field;
	public static JButton btn1,btn2;
	private MainFrame parent;
	private MemberDto user;
	private JDialog dialog;
	private Socket socket;
	public static int messageDialogNum=0;
	private ProtocolRequestEventHandler eventHandler = 
							ProtocolRequestEventHandler.getEventHandler();
	private MyMessengerInfo myInfo = MyMessengerInfo.getInstance();
	public MessageDialog(MainFrame parent , MemberDto user){
		super(parent);
		this.parent = parent;
		this.user = user;
		this.socket = parent.getSocket();
		initAWTEvent();
		messageDialogNum++;
		System.out.println("message reference count : "+messageDialogNum);
		
	}
	public MessageDialog(JDialog dialog , MemberDto user){
		super(dialog);
		this.dialog = dialog;
		this.user = user;
		initAWTEvent();
	}
	private void initAWTEvent(){
		container = this.getContentPane();
		northCenterP = new JPanel();
		northCenterP.setLayout(new BorderLayout(10, 10));
		northWestP = new JPanel(true);
		northNP = new JPanel();
		northEastP = new JPanel();
		//////////////////////////////
		northPanel = new JPanel();
		northPanel.setLayout(new BorderLayout());
		northPanel.add(northNP,BorderLayout.NORTH);
		northPanel.add(northWestP,BorderLayout.WEST);
		northPanel.add(northEastP,BorderLayout.EAST);
		northPanel.add(northCenterP,BorderLayout.CENTER);
		
		centerPanel = new JPanel();
		southPanel = new JPanel();
		southPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		btn1 = new JButton("받는사람");
		btn1.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		btn1.setPreferredSize(new Dimension(90, 20));
		btn2 = new JButton("보내기");
		btn2.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		btn2.setPreferredSize(new Dimension(90, 20));
		field = new JTextField(10);
		field.setText(user.getMemId()+"("+user.getNickName()+")");
		northCenterP.add(btn1,BorderLayout.WEST);
		northCenterP.add(field ,BorderLayout.CENTER);
		area = new JTextArea();
		area.setFocusable(true);
		pane = new JScrollPane(area, 
							   JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
							   JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pane.setPreferredSize(new Dimension(340,180));
		southPanel.add(btn2);
		centerPanel.add(pane);
		container.add(northPanel ,BorderLayout.NORTH);
		container.add(centerPanel,BorderLayout.CENTER);
		container.add(southPanel,BorderLayout.SOUTH);
		
		this.setTitle("쪽지 쓰기");
		this.setBounds(300,200,350,280);
		this.setVisible(true);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		addListener();
	}
	
	private void addListener(){
		btn1.addActionListener(this);
		btn2.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		Object cmd = e.getSource();
		if(cmd==btn1){
			if(area.getText().equals("")){
				JOptionPane.showMessageDialog(null, "글자 입력후 실행해주세요..");
			}else{
				//parent.MessageMap.put(messageDialogNum, this);
				String myId = myInfo.getUser().getMemId();
				MessageProtocol protocol = new MessageProtocol();
				protocol.setProtocolData("protocol", Protocol.REQ_MSG_FRIEND_LIST);
				protocol.setProtocolData("msgNum", messageDialogNum);
				protocol.setProtocolData("id", myId);
				protocol.setProtocolData("socket", MainFrame.socket);
				eventHandler.setProtocol(protocol);
				btn1.setEnabled(false);
			}
		}else if(cmd==btn2){
			//한사람한테만 보내기...
			String msg = area.getText();
			String receiver[] = new String[1];
			receiver[0]=user.getMemId();
			MessageProtocol protocol = new MessageProtocol();
			protocol.setProtocolData("protocol", Protocol.SEND_MESSAGE);
			protocol.setProtocolData("id", myInfo.getUser().getMemId());
			protocol.setProtocolData("name", myInfo.getName());
			protocol.setProtocolData("nick", myInfo.getNickName());
			protocol.setProtocolData("user", receiver);
			protocol.setProtocolData("message", msg);
			protocol.setProtocolData("socket",MainFrame.socket);
			eventHandler.setProtocol(protocol);
			this.dispose();
		}
	}
}
