package uclick.messenger.client.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import uclick.messenger.client.ClientFileServer;
import uclick.messenger.client.ClientResponseReceiver;
import uclick.messenger.client.MyMessengerInfo;
import uclick.messenger.client.ProtocolRequestEventHandler;
import uclick.messenger.client.protocol.MessageProtocol;
import uclick.messenger.client.protocol.Protocol;
import uclick.messenger.client.trayicon.MessengerTrayIcon;


public class MainFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8122142765556432630L;
	private final Color initColor = new Color(245,245,244);
	public Container con;
	public static Socket socket;
	
	private ProtocolRequestEventHandler eventHandler;
	public LoginPanel loginPanel ;
	private MessengerTrayIcon trayIconTest;
	private MenuBar bar;
	public Map<Integer, JDialog> MessageMap = new HashMap<Integer, JDialog>();
	public HashMap<String, Object> menuInfo = new HashMap<String, Object>();
	public boolean isOnLine = false;
	public ClientFileServer clientFileServer;
	public MainFrame(){
		setupNetwork();
		trayIconTest = new MessengerTrayIcon(this);
		eventHandler = ProtocolRequestEventHandler.getEventHandler();
	}
	public void setupNetwork() {
		try {
			socket = new Socket("localhost",7777);
			new ClientResponseReceiver(socket , this).start();
		} catch (Exception e) {
			//JOptionPane.showMessageDialog(this, "네크워크 장애");
			//System.exit(0);
			e.printStackTrace();
		}
	}
	private void initGUI(){
		//changeLookAndFeel(ChatConstants.WINDOWS);
		con = this.getContentPane();
		loginPanel = new LoginPanel(this);
		bar = new MenuBar();
		this.setJMenuBar(bar);
		con.add(loginPanel ,BorderLayout.CENTER);
		this.setTitle(":: UCLICK MESSENGER ::");
		changeLookAndFeel(ChatConstants.GTK);
		this.addWindowListener(new WindowAdapter() {
			
			public void windowOpened(WindowEvent e) {
				
			}
			public void windowClosed(WindowEvent e) {
				MainFrame.this.setVisible(false);
				trayIconTest.logmenuStatus(false);
			}
		});
		this.setBounds(930, 0,300, 500);
		this.setBackground(new Color(161,211,233));
		this.setVisible(true);
		this.setResizable(false);
	}
	
	public Socket getSocket() {
		return socket;
	}
	public MessengerTrayIcon getTrayIconTest() {
		return trayIconTest;
	}
	private void changeLookAndFeel(String looks)
	{
		try {
			UIManager.setLookAndFeel(looks);
		} catch (Exception ex) {
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			}
			catch(Exception exc) {}
		}
		SwingUtilities.updateComponentTreeUI(this);
	}
	class MenuBar extends JMenuBar implements ActionListener , MouseListener{
		
		private static final long serialVersionUID = -4881020993046639013L;
		
		private String menuName[] = {"파일","내정보","help"};
		private String fileMenu[] = {"로그아웃","종료"};	
		private String myMenu[] = {"대화명변경","친구"};
		private String mySubMenu[]={"추가","삭제","그룹이동"};
		private String helpmenu[]={"about"};
		private MyMessengerInfo myInfo = MyMessengerInfo.getInstance();
		
		public MenuBar(){
			initAWTContainer();
		}
		private void initAWTContainer(){
			JMenu menu = null;
			for (int i = 0; i < menuName.length; i++) {
				menu = new JMenu(menuName[i]);
				menu.setBackground(initColor);
				menuInfo.put(menuName[i], menu);
				if(menuName[i].equals("파일")){
					createSubMenu(menu, fileMenu);
					menu.addActionListener(this);
					this.add(menu);
				}else if(menuName[i].equals("내정보")){
					createSubMenu(menu, myMenu);
					menu.addActionListener(this);
					menu.setEnabled(isOnLine);
					this.add(menu);
				}else if(menuName[i].equals("help")){
					createSubMenu(menu, helpmenu);
					menu.addActionListener(this);
					this.add(menu);
				}
			}
			this.setBackground(new Color(245,245,244));
		}
		
		private void createSubMenu(JMenu menu ,String[] data){
			for (int j = 0; j < data.length; j++) {
				if(data[j].equals("친구")){
					JMenu jMenu = new JMenu(data[j]);
					menuInfo.put(data[j], jMenu);
					createSubMenu(jMenu, mySubMenu);//리커션
					jMenu.addActionListener(this);
					menu.add(jMenu);
				}else if(data[j].equals("로그아웃")){
					JMenuItem item = new JMenuItem(data[j]);
					menuInfo.put(data[j], item);
					item.setEnabled(false);
					item.addActionListener(this);
					menu.add(item);
				}else{
					JMenuItem item = new JMenuItem(data[j]);
					menuInfo.put(data[j], item);
					item.addActionListener(this);
					menu.add(item);
				}
			}
		}
		public void actionPerformed(ActionEvent e) {
			String menuName = e.getActionCommand();
			if(menuName.equals("로그아웃")){
				MessageProtocol protocol = new MessageProtocol();
				protocol.setProtocolData("protocol", Protocol.REQ_LOG_OUT);
				protocol.setProtocolData("socket", MainFrame.this.getSocket());
				protocol.setProtocolData("id",myInfo.getUser().getMemId() );
				eventHandler.setProtocol(protocol);
			}else if(menuName.equals("대화명변경")){
				String oldNick = myInfo.getNickName();
				Object obj = JOptionPane.showInputDialog(MainFrame.this, 
														 "변경할 대화명을 입력해주세요..", 
														 "대화명 변경", 
														 JOptionPane.INFORMATION_MESSAGE, 
														 null, 
														 null, 
														 oldNick);
				String newNick = (String)obj;
				if(!isNull(newNick)){
					MessageProtocol protocol = new MessageProtocol();
					protocol.setProtocolData("socket", MainFrame.socket);
					protocol.setProtocolData("protocol", Protocol.REQ_CHANGE_NICK);
					protocol.setProtocolData("myid",myInfo.getUser().getMemId());
					protocol.setProtocolData("oldnick",oldNick);
					protocol.setProtocolData("newnick",newNick);
					eventHandler.setProtocol(protocol);
				}
				
			}else if(menuName.equals("추가")){
				System.out.println("추가진입!!");
				//String oldNick = myInfo.getNickName();
				Object obj = JOptionPane.showInputDialog(MainFrame.this, 
														 "친구요청할 아이디을 입력해주세요..", 
														 "친구 추가", 
														 JOptionPane.INFORMATION_MESSAGE, 
														 null, 
														 null, 
														 "아이디를 입력해주세요");
				String friendId = (String)obj;
				if(!isNull(friendId)){
					MessageProtocol protocol = new MessageProtocol();
					protocol.setProtocolData("socket", MainFrame.this.socket);
					protocol.setProtocolData("protocol", Protocol.REQ_ADD_FRIEND);
					protocol.setProtocolData("myid",myInfo.getUser().getMemId());
					protocol.setProtocolData("myname",myInfo.getUser().getName());
					protocol.setProtocolData("mynick",myInfo.getUser().getNickName());
					protocol.setProtocolData("friendid",friendId);
					eventHandler.setProtocol(protocol);
				}else{
					System.out.println("친구아이디 입력X");
				}
			}else if(menuName.equals("종료")){
				MessageProtocol protocol = new MessageProtocol();
				protocol.setProtocolData("protocol", Protocol.REQ_LOG_OUT);
				protocol.setProtocolData("socket", MainFrame.this.getSocket());
				protocol.setProtocolData("id",myInfo.getUser().getMemId() );
				eventHandler.setProtocol(protocol);
				System.exit(0);
			}
		}
		private boolean isNull(String str){
			return str==null || str.trim().equals("");
		}
		public void mouseReleased(MouseEvent e) {}
		public void mouseClicked(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
		public void mousePressed(MouseEvent e) {}
	}
	public void initMenuSetting(boolean b){
		JMenu m = (JMenu) MainFrame.this.menuInfo.get("내정보");
		JMenuItem mi = (JMenuItem) MainFrame.this.menuInfo.get("로그아웃");
		mi.setEnabled(b);
		m.setEnabled(b);
		m.updateUI();
		m.repaint();
	}
	public static void main(String[] ar){
		EventQueue.invokeLater(new Runnable() {

			public void run() {
				MainFrame frame=new MainFrame();
				frame.initGUI();
			}
		});
	}
}
