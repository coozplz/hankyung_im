package uclick.messenger.client.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ToolTipManager;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import uclick.messenger.client.MyMessengerInfo;
import uclick.messenger.client.ProtocolRequestEventHandler;
import uclick.messenger.client.dto.MemberDto;
import uclick.messenger.client.group.FriendGroup;
import uclick.messenger.client.group.GroupTreeManager;
import uclick.messenger.client.protocol.MessageProtocol;
import uclick.messenger.client.protocol.Protocol;


public class UserTreePanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7880997351335598882L;
	private JPanel leftPanel;
	public JTree userTree;
	public DefaultTreeModel treeModel;
	private DefaultMutableTreeNode rootNode;
	private MainFrame mainFrame;
	public Hashtable<String, DefaultMutableTreeNode> nodeTable = new Hashtable<String, DefaultMutableTreeNode>();
	private GroupTreeManager treeManager;// = GroupTreeManager.getInstance();
	private ProtocolRequestEventHandler eventHandler = 
										ProtocolRequestEventHandler.getEventHandler();
	private MyMessengerInfo myInfo=MyMessengerInfo.getInstance();
	public UserTreePanel(MainFrame mainFrame , GroupTreeManager treeManager) {
		this.mainFrame = mainFrame;
		this.treeManager = treeManager;
		initSwingProperties();
		initAWTContainer();
	}
	private void initAWTContainer(){
		
		this.setLayout(new FlowLayout(FlowLayout.RIGHT));
		createNode();
		userTree = new JTree(treeModel);
		
		userTree.setRowHeight(20);
		userTree.setRootVisible(true);
		userTree.getSelectionModel().setSelectionMode
						(TreeSelectionModel.SINGLE_TREE_SELECTION);
		userTree.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		
		ToolTipManager.sharedInstance().registerComponent(userTree);
		
		userTree.setCellRenderer(new MyRenderer());
		userTree.addMouseListener(new MyMouseAdapter(mainFrame,userTree));
		leftPanel = new JPanel();
		leftPanel.setPreferredSize(new Dimension(30, 300));
		leftPanel.setBorder(BorderFactory.createLineBorder(SystemColor.white));
		JScrollPane scrollpane=new JScrollPane(userTree,
								   JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
								   JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED );
		//scrollpane.setVerticalScrollBar()
		scrollpane.setPreferredSize(new Dimension(250,300));
		scrollpane.setBorder(BorderFactory.createLineBorder(SystemColor.white));
		this.add(leftPanel);
		this.add(scrollpane);
		this.setBackground(SystemColor.white);
		
		expandPath();
		
	}
	private void initSwingProperties(){
		UIDefaults defaults = UIManager.getDefaults();
		defaults.put("scrollBar", Color.red);
	}
	private void createNode(){
		DefaultMutableTreeNode member = null;
		DefaultMutableTreeNode group = null;
		rootNode = new DefaultMutableTreeNode("그룹정보");
		FriendGroup friendGroup = null; 
		for (int i = 0; i < treeManager.getGroupSize(); i++) {
			friendGroup = treeManager.getGroup(i);
			group = new DefaultMutableTreeNode(friendGroup);
			nodeTable.put(((FriendGroup)friendGroup).toString(),group);
			if(friendGroup.getMemberList().size()==0){
				rootNode.add(group);
			}else{
				for (int j = 0; j < friendGroup.getMemberList().size(); j++) {
					member = new DefaultMutableTreeNode(friendGroup.getMember(j));
					nodeTable.put(((MemberDto)friendGroup.getMember(j)).toString(),member);
					group.add(member);
					rootNode.add(group);
				}
			}
			treeModel = new DefaultTreeModel(rootNode);
		}
	}
	private void expandPath(){
		int row = 0;
		for (int i = 0; i < rootNode.getChildCount(); i++) {
			row++;
			userTree.expandRow(row);
			row+=((DefaultMutableTreeNode)rootNode.getChildAt(i)).getChildCount();
		}
	}
	
	public void updateChangeNick(String oldNick,String newnick , String memId){ 
		DefaultMutableTreeNode node=null;
		DefaultMutableTreeNode parentNode=null;
		MemberDto user = new MemberDto();
		user.setNickName(oldNick);
		user.setMemId(memId);
		node=nodeTable.get(user.toString());
		parentNode = (DefaultMutableTreeNode) node.getParent();
		int childIndex = parentNode.getIndex(node);
		parentNode.remove(childIndex);
		user.setNickName(newnick);
		user.setStatus("1");
		node = new DefaultMutableTreeNode(user);
		nodeTable.remove(oldNick);
		nodeTable.put(user.toString(), node);
		parentNode.insert(node, childIndex);
		node.setUserObject(user);
		updateUser(user);
	}
	public void logoutUser(String nickname){
	  MemberDto user=new MemberDto();
	  user.setNickName(nickname);
	  DefaultMutableTreeNode node=nodeTable.get(user.toString());	
	  DefaultMutableTreeNode parentNode=(DefaultMutableTreeNode)node.getParent();
	  
	  int childindex=parentNode.getIndex(node);
	  parentNode.remove(childindex);
	  user.setNickName(nickname);
	  user.setStatus("0");
	  node=new DefaultMutableTreeNode(user.toString());
	  nodeTable.put(user.toString(),node);
	  parentNode.insert(node, childindex);
	  node.setUserObject(user);
	  updateUser(user);
	}
	
	public void addUser(Object child){
		DefaultMutableTreeNode childNode =
								new DefaultMutableTreeNode(child);
		treeModel.insertNodeInto(childNode,rootNode,rootNode.getChildCount());
		nodeTable.put(((MemberDto)child).toString(),childNode);
	}
	
	public void removeUser(MemberDto user){
		MutableTreeNode node = 
				(MutableTreeNode)nodeTable.get(user.toString());
		node.setUserObject(user);
		treeModel.reload(node);
	}
	
	public void updateUser(MemberDto user){
		
		DefaultMutableTreeNode node = null;
		node = (DefaultMutableTreeNode)nodeTable.get(user.toString());
		if(node==null){
			addUser(node);
		}
		node.setUserObject(user);
		treeModel.reload(node);
		nodeTable.put(user.toString(),node);
	}
	/**
	 * 정수  그룹 이동
	 * @param userDto
	 * @param moveDto
	 */
	private void removeGroupuser(MemberDto oldUser){
		DefaultMutableTreeNode node = 
					(DefaultMutableTreeNode)nodeTable.get(oldUser.toString());//old
		DefaultMutableTreeNode parent = 
					(DefaultMutableTreeNode) node.getParent();
		int childIndex = parent.getIndex(node);
		parent.remove(childIndex);
	}
	
	private void insertGroupUser(MemberDto oldUser,MemberDto newUser){
		String newGroup = newUser.getGroupName();
		DefaultMutableTreeNode newParent = nodeTable.get(newGroup);
		DefaultMutableTreeNode newChild  = new DefaultMutableTreeNode(newUser);
		int count = newParent.getChildCount();
		newParent.insert(newChild, count);
		nodeTable.remove(oldUser.toString());
		nodeTable.put(newUser.toString(), newChild);
		updateUser(newUser);
		userTree.updateUI();
		expandPath();
	}
	/**
	 * 정수 그룹 이동
	 * @param oldUser
	 * @param newUser
	 */
	public void updateGroup(MemberDto oldUser, MemberDto newUser){	
		removeGroupuser(oldUser);
		insertGroupUser(oldUser, newUser);
	}
	public void addFriend(MemberDto newUser){
		DefaultMutableTreeNode newNode = null;
		DefaultMutableTreeNode parentNode = null;
		
		newNode = new DefaultMutableTreeNode(newUser);
		
		parentNode = (DefaultMutableTreeNode)rootNode.getChildAt(3);
		parentNode.insert(newNode, parentNode.getChildCount());
							
		nodeTable.put(newUser.toString(), newNode);
		//updateUser(newUser);
		userTree.updateUI();
		expandPath();
	}
	
	public void deleteFriend(MemberDto delUser){
		DefaultMutableTreeNode delNode = null;
		DefaultMutableTreeNode parentNode = null;
		delNode = (DefaultMutableTreeNode)nodeTable.get(delUser.toString());//old
		parentNode = (DefaultMutableTreeNode)delNode.getParent();
		int delNodeIndex = parentNode.getIndex(delNode); 
		parentNode.remove(delNodeIndex);
		nodeTable.remove(delUser.toString());
		//updateUser(newUser);
		userTree.updateUI();
		expandPath();
	}
	/**
	 * 
	 * @author Administrator
	 *
	 */
	class MyMouseAdapter extends MouseAdapter implements ActionListener{
		private MainFrame frame;
		private JTree tree;
		private JPopupMenu menu;
		private JMenuItem name,chatt,file ,message,friendDelete;
		private JMenu friendMove;
		private JMenuItem groupMove;
		private DefaultMutableTreeNode node;
		private String[] groupName ={"가족","친구","직장","기타"};
		int selRow;
		private TreePath selPath;
		public MyMouseAdapter(MainFrame frame,JTree tree)
		{
			this.frame = frame;
			this.tree = tree;
		}
		private void makePopup(MouseEvent e,MemberDto user,String status){
			menu = new JPopupMenu();
			name = new JMenuItem(user.getName()+"("+user.getNickName()+")");
			
			chatt = new JMenuItem("대화 하기");
			file = new JMenuItem("파일 보내기");
			message = new JMenuItem("쪽지보내기");
			if(status.equals("0")){
				chatt.setEnabled(false);
				file.setEnabled(false);
				message.setEnabled(false);
			}
			friendMove = new JMenu("친구 이동");
			friendDelete = new JMenuItem("친구 삭제");
			
			menu.add(name);
			menu.addSeparator();
			menu.add(chatt);
			menu.add(file);
			menu.add(message);
			menu.addSeparator();
			menu.add(friendMove);
			menu.add(friendDelete);
			menu.show(e.getComponent(), e.getX()+2, e.getY());
			addListener();
			
		}
		private void addListener(){
			chatt.addActionListener(this);
			file.addActionListener(this);
			message.addActionListener(this);
			friendDelete.addActionListener(this);
		}
		public void mouseClicked(MouseEvent e) {
			selRow = tree.getRowForLocation(e.getX(), e.getY());
			selPath = tree.getPathForLocation(e.getX(), e.getY());
			try{
				if(selRow > 0 ) {
					if(e.getClickCount() == 2) {
						DefaultMutableTreeNode node = (DefaultMutableTreeNode)selPath.getLastPathComponent();
						MemberDto user = (MemberDto)(node.getUserObject());
						System.out.println(user.getStatus());
						if(!user.getStatus().equals("0")) {//값 변경해야함......
							MessageProtocol protocol = new MessageProtocol();
							protocol.setProtocolData("protocol", Protocol.REQ_MAKE_ROOM);
							protocol.setProtocolData("myId",myInfo.getUser().getMemId());
							protocol.setProtocolData("friendId", user.getMemId());
							System.out.println("friend nickname : "+user.getNickName());
							protocol.setProtocolData("socket", mainFrame.getSocket());
							eventHandler.setProtocol(protocol);
						} else {
							
							JOptionPane.showMessageDialog(frame,
									"Click on an online user to send a message",
									"Error",JOptionPane.INFORMATION_MESSAGE);
						}
					}
				}
			}catch (Exception ex) {
				//ex.printStackTrace();
			}
		}
		 public void mousePressed(MouseEvent e) { 
			 maybeShowPopup(e); 
	     } 
	     public void mouseReleased(MouseEvent e) { 
	         maybeShowPopup(e); 
	    } 
	    
		private void maybeShowPopup(MouseEvent e) { 
			if (e.isPopupTrigger()) {
				try{
					if (selPath != null) {
						TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
						DefaultMutableTreeNode selNode = (DefaultMutableTreeNode)(selPath.getLastPathComponent());
						if (selNode != rootNode) {
							
							tree.setSelectionPath(selPath);
							MemberDto user = (MemberDto)(selNode.getUserObject());
							makePopup(e , user ,user.getStatus());
							createGroupInfoMenu(selNode.getParent().toString());
						}
					}
				}catch (Exception ex) {
					
				}
			}
		}
		private void createGroupInfoMenu(String node){
			for (int i = 0; i < groupName.length; i++) {
				groupMove = new JMenuItem(groupName[i]);
				if(groupName[i].equals(node)){
					groupMove.setEnabled(false);
					
				}
				groupMove.addActionListener(this);
				friendMove.add(groupMove);
			}
		}
		/**
		 * 팝업창에 대한 이벤트 리스너..프로토콜 작업 해야함....
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			node = (DefaultMutableTreeNode)selPath.getLastPathComponent();
			MemberDto user = (MemberDto)(node.getUserObject());
			String popupName = e.getActionCommand();
			
			if(popupName.equals("대화 하기")){
				
				MessageProtocol protocol = new MessageProtocol();
				protocol.setProtocolData("protocol", Protocol.REQ_MAKE_ROOM);
				protocol.setProtocolData("myId",myInfo.getUser().getMemId());
				protocol.setProtocolData("friendId", user.getMemId());
				protocol.setProtocolData("socket", mainFrame.getSocket());
				eventHandler.setProtocol(protocol);
				
			}else if(popupName.equals("파일 보내기")){
				Vector<String> receiverList = new Vector<String>(5,5);
				receiverList.add(user.getMemId());
				
				MessageProtocol protocol = new MessageProtocol();
				protocol.setProtocolData("protocol", Protocol.FILE_TRANSFER_INIT);
				protocol.setProtocolData("sender",myInfo.getUser().getMemId());
				protocol.setProtocolData("roomnum", "-1");
				protocol.setProtocolData("receiverlist", receiverList);
				protocol.setProtocolData("socket", mainFrame.getSocket());
				eventHandler.setProtocol(protocol);
				
				
			}else if(popupName.equals("쪽지보내기")){
				
				new MessageDialog(mainFrame, user);
				
			}else if(popupName.equals("친구 삭제")){
			
				deleteFriend(myInfo.getUser().getMemId(), user.getMemId(), user.getNickName());
				
			}else if(popupName.equals("친구")){
				
				group_change(myInfo.getUser().getMemId(), user.getMemId(), user.getNickName(), popupName);
				
			}else if(popupName.equals("직장")){
				
				group_change(myInfo.getUser().getMemId(), user.getMemId(), user.getNickName(), popupName);
				
			}else if(popupName.equals("가족")){
				
				group_change(myInfo.getUser().getMemId(), user.getMemId(), user.getNickName(), popupName);
				
			}else if(popupName.equals("기타")){
				
				group_change(myInfo.getUser().getMemId(), user.getMemId(), user.getNickName(), popupName);
				
			}else{
				//그룹명에 대한 프로토콜 작업
				System.out.println("groupName : "+popupName);
			}
			
		}
		private void deleteFriend(String m_id, String f_id, String nickname){
			MessageProtocol protocol = new MessageProtocol();
			protocol.setProtocolData("socket", mainFrame.getSocket());
			protocol.setProtocolData("protocol", Protocol.REQ_DELETE_FRIEND);
			protocol.setProtocolData("myId",m_id);
			protocol.setProtocolData("friendId", f_id);
			protocol.setProtocolData("friendNick", nickname);
			eventHandler.setProtocol(protocol);
		}
		private void group_change(String m_id, String f_id, String nickName, String popupName){
			MessageProtocol protocol = new MessageProtocol();
			
			protocol.setProtocolData("socket", mainFrame.getSocket());
			protocol.setProtocolData("protocol", Protocol.REQ_CHANGE_GROUP);
			protocol.setProtocolData("myId",m_id);
			protocol.setProtocolData("friendId", f_id);
			protocol.setProtocolData("friendNick", nickName);
			protocol.setProtocolData("group_name", popupName);
			eventHandler.setProtocol(protocol);
		}
		
	}
	class MyRenderer extends DefaultTreeCellRenderer implements ChatConstants{
		/**
		 * 
		 */
		private static final long serialVersionUID = 8381504023021052067L;
		ClassLoader loader;// = 
		ImageIcon rootIcon,onlineIcon,offlineIcon,busyIcon,idleIcon;
						
	
		public MyRenderer(){
				loader = Thread.currentThread().getContextClassLoader();
				if(loader == null){
					loader  = ClassLoader.getSystemClassLoader();
				}
				rootIcon = new ImageIcon("image/root.gif");
				onlineIcon = new ImageIcon("image/online.gif");
				offlineIcon = new ImageIcon("image/offline.gif");
				busyIcon = new ImageIcon("image/busy.gif");
				idleIcon = new ImageIcon("image/idle.gif");
		}
		public Component getTreeCellRendererComponent(
							JTree tree,
							Object value,
							boolean sel,
							boolean expanded,
							boolean leaf,
							int row,
							boolean hasFocus) {
	
			super.getTreeCellRendererComponent(
							tree, value, sel,
							expanded, leaf, row,
							hasFocus);
	
	
			if (leaf) {
				MemberDto user=getUser(value);
				if(user==null){
					setIcon(rootIcon);//ROOT
					setToolTipText(null);
				}else{
					switch(Integer.parseInt(user.getStatus())) {
					case ONLINE :
						setIcon(onlineIcon);//ONLINE USER
						setToolTipText("online");
						break;
					case OFFLINE:
						setIcon(offlineIcon);//OFFLINE
						setToolTipText("Offline");
						break;
					case BUSY :
						setIcon(busyIcon);//BUSY USER
						setToolTipText("I am busy");
						break;
					case IDLE :
						setIcon(idleIcon);//IDLE USER
						setToolTipText("Away from computer");
						break;
					default:
						setIcon(offlineIcon);//OFFLINE
						setToolTipText("Offline");
					}
				}
				
			} else {
				setIcon(rootIcon);//최상위 그룹정보ROOT
				setToolTipText(null);
			}
			return this;
		}
		private MemberDto getUser(Object value){
			DefaultMutableTreeNode node =(DefaultMutableTreeNode)value;
			try{
				MemberDto nodeInfo =(MemberDto)(node.getUserObject());
				return nodeInfo;
			}catch (Exception e) {
				return null;
			}
		}
	}
	public Socket getSocket(){
		return mainFrame.getSocket();
	}
}





