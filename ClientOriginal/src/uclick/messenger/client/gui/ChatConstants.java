package uclick.messenger.client.gui;

import java.awt.Color;

public interface ChatConstants {

	final int OFFLINE		= 0;
	final int ONLINE		= 1;
	final int BUSY			= 3;
	final int IDLE			= 4;
	
	final String WINDOWS ="com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
	final String METAL   ="javax.swing.plaf.metal.MetalLookAndFeel";
	final String MOTIF   = "com.sun.java.swing.plaf.motif.MotifLookAndFeel";
	final String GTK 			 ="com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
	
	final Color initColor = new Color(245,245,244);
	final Color userColor = new Color(162,211,234);
}
