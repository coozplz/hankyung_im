package uclick.messenger.client.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import uclick.messenger.client.MyMessengerInfo;
import uclick.messenger.client.dto.MemberDto;

public class ReceiveMessageDialog extends JDialog implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = -634540264177500982L;
	private static final int MAX_INTERVAL = 3;
	private Container container;
	private JPanel northCenterP,northWestP,northEastP,northNP;
	private JPanel northPanel;
	private JPanel centerPanel;
	private JPanel southPanel;
	private JScrollPane pane;
	private JTextArea area;
	private JLabel sendLabel = new JLabel("보낸사람 :");
	private JLabel sender;
	private JLabel receiverLable = new JLabel("받는사람 :");
	private JLabel receiver;
	
	public static JButton receiveMessage;
	private MainFrame parent;
	private MemberDto user;
	private Timer timer;
	private MyMessengerInfo myInfo = MyMessengerInfo.getInstance();
	private String message;
	
	public ReceiveMessageDialog(MainFrame parent , MemberDto user,String message){
		super(parent);
		this.parent = parent;
		this.user = user;
		this.message = message;
		initAWTContainer();
	}
	
	private void initAWTContainer(){
		container = this.getContentPane();
		northCenterP = new JPanel();
		northCenterP.setLayout(new GridLayout(2, 1, 5, 2));
		northWestP = new JPanel(true);
		northWestP.setLayout(new GridLayout(2, 1, 5, 2));
		northNP = new JPanel();
		northEastP = new JPanel();
		//////////////////////////////
		northPanel = new JPanel();
		northPanel.setLayout(new BorderLayout(15, 0));
		northPanel.add(northNP,BorderLayout.NORTH);
		northPanel.add(northWestP,BorderLayout.WEST);
		northPanel.add(northEastP,BorderLayout.EAST);
		northPanel.add(northCenterP,BorderLayout.CENTER);
		
		centerPanel = new JPanel();
		southPanel = new JPanel();
		
		southPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		receiveMessage = new JButton("답장");
		receiveMessage.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		receiveMessage.setPreferredSize(new Dimension(60, 20));

		sendLabel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		receiverLable.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		sender = new JLabel();
		sender.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		sender.setText(user.getName()+"( "+getcurrentDate()+" )");
		
		receiver = new JLabel();
		receiver.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
		receiver.setText(myInfo.getName());
		northWestP.add(sendLabel);
		northWestP.add(receiverLable);
		northCenterP.add(sender);
		northCenterP.add(receiver);
		area = new JTextArea();
		area.setFocusable(true);
		area.setEditable(false);
		area.setText(message);
		pane = new JScrollPane(area, 
							   JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
							   JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		pane.setPreferredSize(new Dimension(340,175));
		
		southPanel.add(receiveMessage);
		centerPanel.add(pane);
		container.add(northPanel ,BorderLayout.NORTH);
		container.add(centerPanel,BorderLayout.CENTER);
		container.add(southPanel,BorderLayout.SOUTH);
		
		this.setTitle(user.getName()+"님의 쪽지");
		this.setBounds(300,200,350,280);
		this.setVisible(true);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setAlwaysOnTop(true);
		addListener();
		setMessageTimer();
		area.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent e) {
				System.out.println("mouse cancle");
				timer.cancel();
			}
		});
		this.addWindowListener(new WindowAdapter() {
			
			public void windowClosing(WindowEvent e) {
				System.out.println("1cancle");
				timer.cancel();
			}
			public void windowClosed(WindowEvent e) {
				System.out.println("2cancle");
				timer.cancel();
			}
		});
	}
	private void setMessageTimer(){
		timer = new Timer();
		timer.schedule(new TimerTask() {
			int count = 0;
			public void run() {
				if(count==MAX_INTERVAL){
					ReceiveMessageDialog.this.dispose();
					this.cancel();
				}
				count++;
			}
		}, 0, 5000);
	}
	private String getcurrentDate(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy년 MM월 dd일 hh:mm:ss");
		return sdf.format(new Date());
	}
	private void addListener(){

		receiveMessage.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) {
		Object cmd = e.getSource();
		
		if(cmd==receiveMessage){
			this.dispose();
			MessageDialog dialog = new MessageDialog(this, user);
			parent.MessageMap.put(MessageDialog.messageDialogNum, dialog);
		}
	}
}
