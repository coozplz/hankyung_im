package uclick.messenger.client.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import uclick.messenger.client.ProtocolRequestEventHandler;
import uclick.messenger.client.dto.MemberDto;
import uclick.messenger.client.protocol.MessageProtocol;
import uclick.messenger.client.protocol.Protocol;


public class LoginPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6896097214024028896L;
	private MainFrame frame;
	private JPanel centerP = new JPanel();
	private JPanel loginPanel = new JPanel();
	private JPanel jPanel3 = new JPanel();
	
	private JButton loginBtn = new JButton("로그인");
	private JButton regiBtn = new JButton("회원가입");
	private JLabel jLabel1 = new JLabel("아이디");
	private JLabel jLabel2 = new JLabel("비밀번호");
	private JTextField text1 = new JTextField(10);
	private JPasswordField password = new JPasswordField(10);
	private ProtocolRequestEventHandler eventHandler = ProtocolRequestEventHandler.getEventHandler();
	public LoginPanel(MainFrame frame ){
		this.frame = frame;
		initAWTContainer();
	}
	private void initAWTContainer(){
		
		this.setLayout(new BorderLayout());
		this.setBackground(SystemColor.white);
		this.setBorder(BorderFactory.createEtchedBorder());
		this.setVisible(true);
		
		centerP.setLayout(null);
		loginPanel.setBounds(30, 120, 220, 90);
		loginPanel.setLayout(new BorderLayout());
		loginPanel.setBackground(SystemColor.black);
		jPanel3.setLayout(new GridLayout(3,3,5,5));
		jPanel3.setBackground(new Color(245,245,244));
		jPanel3.add(jLabel1);
		jPanel3.add(text1);
		jPanel3.add(jLabel2);
		jPanel3.add(password);
		jPanel3.add(loginBtn);
		jPanel3.add(regiBtn);
		loginPanel.add(jPanel3);
		centerP.add(loginPanel);
		centerP.setBackground(ChatConstants.initColor);
		this.add(centerP, BorderLayout.CENTER);
		
		loginBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(e.getActionCommand().equals("로그인")){
					
					MessageProtocol protocol = new MessageProtocol();
					MemberDto dto = new MemberDto();
					String id = text1.getText();
					String passwd = new String(password.getPassword());
					dto.setMemId(id);
					dto.setPassword(passwd);
					protocol.setProtocolData("socket", MainFrame.socket);
					protocol.setProtocolData("protocol", Protocol.REQ_LOGIN);
					protocol.setProtocolData("member", dto);
					eventHandler.setProtocol(protocol);
				}
			}
		});
		regiBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				try {
					String[] cmd = new String[4];
					cmd[0] = "cmd.exe";
					cmd[1] = "/C";
					cmd[2] = "start";
					cmd[3] = "http://www.javaworld.com/javaworld/javatips/jw-javatip66.html";
					Process process = Runtime.getRuntime().exec( cmd );
										
				} catch (IOException e1) {
					
					e1.printStackTrace();
				}
			}
		});
	}
}
