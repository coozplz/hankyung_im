package uclick.messenger.client;

import uclick.messenger.client.dto.MemberDto;

public class MyMessengerInfo {

	private static MyMessengerInfo Instance;
	
	private MemberDto user;
	private MyMessengerInfo() {
		user =new MemberDto();
	}
	public static MyMessengerInfo getInstance() {
		if(Instance==null){
			Instance = new MyMessengerInfo();
		}
		return Instance;
	}
	public MemberDto getUser() {
		return user;
	}
	public void setUser(MemberDto user) {
		this.user = user;
	}
	public String getName(){
		return this.user.getName();
	}
	public String getNickName(){
		return this.user.getNickName();
	}
	public void setNickName(String nickName){
		 this.user.setNickName(nickName);
	}
}
