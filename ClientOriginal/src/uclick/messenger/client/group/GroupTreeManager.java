package uclick.messenger.client.group;

import java.util.Vector;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import uclick.messenger.client.MyMessengerInfo;
import uclick.messenger.client.dto.MemberDto;


public class GroupTreeManager {

	private static GroupTreeManager Instance;
	private Vector<FriendGroup> friendGroupManager;
	private MyMessengerInfo myInfo = MyMessengerInfo.getInstance();
	public GroupTreeManager() {
		friendGroupManager = new Vector<FriendGroup>();
	}
	
	public static GroupTreeManager getInstance() {
		if(Instance== null){
			Instance = new GroupTreeManager();
		}
		return Instance;
	}

	public void addGroup(FriendGroup friendGroup){
		this.friendGroupManager.add(friendGroup);
	}
	public int getGroupSize(){
		return friendGroupManager.size();
	}
	public String getGroupName(int index){
		FriendGroup friendGroup = friendGroupManager.elementAt(index);
		System.out.println("group name : "+ friendGroup.getGroupName());
		return friendGroup.getGroupName();
	}
	public FriendGroup getGroup(int index){
		FriendGroup friendGroup = friendGroupManager.elementAt(index);
		return friendGroup;
	}
	public void createGroup(Vector<MemberDto> userList , String groupName){
		FriendGroup friendGroup = null;
		for (int i = 0; i < userList.size(); i++) {
			friendGroup = new FriendGroup(groupName);
			friendGroup.addUser(userList.elementAt(i));
		}
		friendGroupManager.add(friendGroup);
	}
	
	public Vector<FriendGroup> getFriendGroupManager() {
		return friendGroupManager;
	}
	public void createUserGroup(Document document ,String[] grouplist){
		FriendGroup friendGroup = null;
		Vector<MemberDto> userList = getGroupList(document, "user" , true);
		Vector<MemberDto> groupUserList = null;
		for (int i = 0; i < grouplist.length; i++) {
			friendGroup = new FriendGroup(grouplist[i]);
			for (int j = 0; j < userList.size(); j++) {
				if(userList.get(j).getGroupName().equals(grouplist[i])){
					groupUserList = new Vector<MemberDto>();
					groupUserList.add(userList.get(j));
					friendGroup.mathingFriendList(groupUserList);
				}
			}
			addGroup(friendGroup);
		}
	}
	public Vector<MemberDto> getGroupList(Document document , String tagName , boolean flag ){
		Vector<MemberDto> userList = new Vector<MemberDto>();
		NodeList parent = document.getElementsByTagName(tagName);
		
		for (int i = 0; i < parent.getLength(); i++) {
			NodeList child = parent.item(i).getChildNodes();
			MemberDto dto = new MemberDto();
			for (int j = 0; j < child.getLength(); j++) {
				String element = child.item(j).getTextContent();
				String nodeName = child.item(j).getNodeName();
				if(nodeName.equals("id")){
					dto.setMemId(element);
				}else if(nodeName.equals("name")){
					dto.setName(element);
				}else if(nodeName.equals("nickname")){
					dto.setNickName(element);
				}else if(nodeName.equals("status")){
					dto.setStatus(element);
				}else if(nodeName.equals("groupname")){
					dto.setGroupName(element);
				}
			}
			userList.add(dto);
		}
		if(flag){
			myInfo.setUser(filteringMyInfo(userList));
		}
		return userList;
	}
	public MemberDto filteringMyInfo(Vector<MemberDto> list){
		MemberDto dto = null;
		for (int i = 0; i < list.size(); i++) {
			if(list.get(i).getGroupName().equals("me")){
				dto =  list.get(i);
				list.remove(i);
				return dto;
			}
		}
		return null;
	}
}
