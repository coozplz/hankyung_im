package uclick.messenger.client.dto;

public class FileDto {
	String filename;
	long filesize;
	String member;
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public long getFilesize() {
		return filesize;
	}
	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}
	public String getMember() {
		return member;
	}
	public void setMember(String member) {
		this.member = member;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("FileDto [filename=").append(filename).append(
				", filesize=").append(filesize).append(", member=").append(
				member).append("]");
		return builder.toString();
	}
	

}
