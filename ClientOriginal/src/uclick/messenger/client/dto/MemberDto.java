package uclick.messenger.client.dto;

import java.io.Serializable;
import java.net.Socket;

public class MemberDto implements Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7606310082650717592L;
	private String memId;
	private String password;
	private String name;
	private String address;
	private String nickName;
	private String status;
	private String ip;
	private int auth;
	private String groupName;
	//
	private Socket socket;
	public MemberDto() {
		
	}
	public MemberDto(String memId, String name, String address,
			String nickName, String status, String ip, int auth) {
		
		this.memId = memId;
		this.name = name;
		this.address = address;
		this.nickName = nickName;
		this.status = status;
		this.ip = ip;
		this.auth = auth;
	}
	
	public String getMemId() {
		return memId;
	}
	public void setMemId(String memId) {
		this.memId = memId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getAuth() {
		return auth;
	}
	public void setAuth(int auth) {
		this.auth = auth;
	}
	
	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	public Socket getSocket() {
		return socket;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	@Override
	public String toString() {
		return nickName;
	}
	
}
