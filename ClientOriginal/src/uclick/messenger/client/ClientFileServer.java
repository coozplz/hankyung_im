package uclick.messenger.client;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import uclick.messenger.client.dto.FileDto;
import uclick.messenger.client.file.Download;
import uclick.messenger.client.protocol.Protocol;

public class ClientFileServer extends Thread {
	/**
	 * Logger for this class
	 */
	
	ServerSocket serverSocket;
	String serverip; 
	FileDto fileDto;


	public void setFileDto(FileDto fileDto) {
		this.fileDto = fileDto;
	}
	public String getServerip() {
		return serverip;
	}
	Socket socket;
	public ClientFileServer() {
		try {
			InetAddress address = InetAddress.getLocalHost();
			serverip = address.getHostAddress();
			serverSocket = new ServerSocket(Protocol.CLIENT_PORT,0,address);

			start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public  void run() {
		try {
			while (true) {
				socket = serverSocket.accept();
				if (socket != null) {
					new Download(fileDto,socket);
				}
			}
		} catch (IOException e) {
		}
	}
}

