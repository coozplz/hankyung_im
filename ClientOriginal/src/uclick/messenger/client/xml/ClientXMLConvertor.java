package uclick.messenger.client.xml;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.Socket;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ClientXMLConvertor {

	private DocumentBuilderFactory factory;
	private DocumentBuilder builder;
	private TransformerFactory transformerFactory;
	private Transformer transformer;
	private volatile int currentBuffer;
	
	public ClientXMLConvertor() {

		try {
			factory = DocumentBuilderFactory.newInstance();
			factory.setIgnoringElementContentWhitespace(true);
			builder = factory.newDocumentBuilder();
			transformerFactory = TransformerFactory.newInstance();
			transformer = transformerFactory.newTransformer();
			
		} catch (ParserConfigurationException e) {

			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			
			e.printStackTrace();
		}
	}
	public synchronized Document getDoc() {
		return builder.newDocument();
	}

	public synchronized String textFromXML(Document doc, String doctype) {

		StringWriter writer = null;
		Result result = null;
		Source source = null;
		try {
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype);
			transformerFactory.setAttribute("indent-number", new Integer(4));
			transformer.setOutputProperty(OutputKeys.ENCODING, "euc-kr");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			source = new DOMSource(doc);
			writer = new StringWriter();
			result = new StreamResult(writer);

			transformer.transform(source, result);
			return writer.toString();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
		return null;
	}

	public synchronized void writer(String str, Socket socket)
													throws IOException {

		DataOutputStream out = new DataOutputStream(socket.getOutputStream());
		PrintWriter writer = new PrintWriter(socket.getOutputStream());
		StringReader strReader = new StringReader(str);
		try {
			int s = str.length();
			out.writeInt(s);
			System.out.println("문자 길이"+str.length());
			synchronized (out) {
				out.flush();
			}
			while (true) {
				
				char[] buffer = getBufferSize(s);
				int read = strReader.read(buffer);
				if (read == -1 || currentBuffer >= str.length()) {
					break;
				}
				writer.write(buffer, 0, read);
				currentBuffer += read;
			}
			
			writer.flush();
			currentBuffer = 0;

		} catch (Exception e) {
			
		} finally {
			
		}
	}

	private char[] getBufferSize(int fileSize) {
		char buffer[] = new char[fileSize];
		
		return buffer;
	}

	public Document readXML(InputSource input) throws org.xml.sax.SAXException,
			IOException, ParserConfigurationException {
		if (input == null)
			throw new IOException("fromXML: null input");
		return builder.parse(input);
	}
	
	public Document getDocument(String fileName) {
		try {
			File f = new File(fileName);
			if (f.exists()) {
				return readXML(new InputSource(new FileReader(f)));
			} else
				return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public Document toDoc(String requestXML) throws SAXException, IOException {
		StringReader reader = new StringReader(requestXML);
		InputSource source = new InputSource(reader);
		Document document = builder.parse(source);
		return document;
	}
	/**
	 * 
	 * @param document
	 * @param tagName
	 * @param content
	 */
	public void addContent(Document document, String tagName, String content) {
		Document doc = document;
		NodeList list = doc.getElementsByTagName(tagName);
		Element element = (Element) list.item(0);
		element.setTextContent(content);
	}
	public void addMajorContent(Document doc,String parent,String childName,String[]contnet){
		NodeList list=doc.getElementsByTagName(parent);
		Element root=(Element)list.item(0);
		
		for(int i=0;i<contnet.length;i++){
			Element element=doc.createElement(childName);
			Text text=doc.createTextNode(contnet[i]);
			System.out.println("text"+text);
			System.out.println(contnet[i]);
			element.appendChild(text);
			root.appendChild(element);
		}
	}
	/**
	 * document type to string type
	 * 
	 * @param document
	 * @return
	 * @throws TransformerException
	 */
	public String toString(Document document) throws TransformerException {
		String xmlData = null;
		Source source = new DOMSource(document);
		StringWriter stringWriter = new StringWriter();
		Result result = new StreamResult(stringWriter);
		transformer.transform(source, result);
		xmlData = stringWriter.toString();
		return xmlData;
	}
	/**
	 * 
	 * @param document
	 * @param tagName
	 * @return
	 */
	public String getContent(Document document, String tagName) {
		NodeList list = document.getElementsByTagName(tagName);
		Element element = (Element) list.item(0);
		return element.getTextContent();
	}

	public Vector<String> getContentList(Document document, String tagName) {
		Vector<String> strList = new Vector<String>();
		NodeList list = document.getElementsByTagName(tagName);
		for (int i = 0; i < list.getLength(); i++) {
			Element element = (Element) list.item(i);
			strList.add(element.getTextContent());

		}
		return strList;
	}
	public void createElement(Document document,Element parent , String tagName,String content){
		Element child = document.createElement(tagName);
		child.setTextContent(content);
		parent.appendChild(child);
	}
	/**
	 * Document 유효성 체크 메소드(스키마..)
	 */
	public boolean docValidate(String requesetXML, String mathDocType) {
		boolean isValide = false;
		StringReader reader = new StringReader(requesetXML);
		Source xmlSource = new StreamSource(reader);
		Source[] dtdSource = new Source[] { new StreamSource(mathDocType) };
		isValide = XMLValidator.validateXMLSchema(xmlSource, dtdSource);
		return isValide;
	}
}
