package uclick.messenger.client.xml;

import javax.xml.XMLConstants;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;

public class XMLValidator implements ErrorHandler{

	static boolean  isValide;
	/**
	 * XML 유효성 체크
	 * @param source
	 * @return
	 */
	public static boolean validateXML(InputSource source){
		XMLValidator.isValide = true;
	
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setValidating(true);
			factory.setFeature("http://apache.org/xml/feature/validation/schema", true);
			SAXParser parser = factory.newSAXParser();
			XMLReader reader = parser.getXMLReader();
			reader.setErrorHandler(new XMLValidator());
			reader.parse(source);
		} catch (Exception e) {
			XMLValidator.isValide = false;
		}
		return XMLValidator.isValide;
	}
	/**
	 * 스키마 유효성 체크
	 * @param sourceXML
	 * @param sourceDtd
	 * @return
	 */
	public static boolean validateXMLSchema(Source sourceXML , Source[] sourceDtd ){
		XMLValidator.isValide = true;
		try {
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.XML_DTD_NS_URI);
			Schema schema = schemaFactory.newSchema(sourceDtd);
			Validator validator =schema.newValidator();
			validator.setErrorHandler( new XMLValidator());
			validator.validate(sourceXML);
		} catch (Exception e) {
			isValide = false;
		}
		return isValide;
	}

	public void error(SAXParseException exception) throws SAXException {
		System.out.println("error");
	}

	public void fatalError(SAXParseException exception) throws SAXException {
		System.out.println("fatal error");
	}
	public void warning(SAXParseException exception) throws SAXException {
		System.out.println("warning");
	}
}

